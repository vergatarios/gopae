<?php

class ReporteRegistroUnicoCommand extends CConsoleCommand {
    
    
    //ACCION PARA GENERAR LAS TABLAS
    //COMANDOS:
    //*  cd /var/www/gopae/web/protected
    //*  php yiic reporteRegistroUnico estadistico
    
    public function actionEstadistico() {
        
        $fecha = date('Y-m-d');
        $hora = date('H:i:s');
        $accion = "estadistico";
        
        ReporteRegistroUnicoDiario::reporteEstadisticoRegistroUnicoDiario($fecha, $hora, $accion);
    }


}
