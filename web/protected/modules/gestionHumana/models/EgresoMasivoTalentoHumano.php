<?php

/**
 * This is the model class for table "gestion_humana.talento_humano".
 *
 * The followings are the available columns in table 'gestion_humana.talento_humano':
 * @property string $id
 * @property string $origen
 * @property integer $cedula
 * @property string $rif
 * @property string $nombre
 * @property string $apellido
 * @property string $sexo
 * @property string $fecha_nacimiento
 * @property string $foto
 * @property string $telefono_fijo
 * @property string $telefono_celular
 * @property string $telefono_oficina
 * @property string $email_personal
 * @property string $email_corporativo
 * @property string $twitter
 * @property string $certificado_medico
 * @property string $manipulacion_alimentos
 * @property string $registro_militar
 * @property integer $grado_instruccion_id
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property string $direccion
 * @property string $enfermedades
 * @property string $aptitudes
 * @property integer $mision_id
 * @property string $hijo_en_plantel
 * @property integer $cantidad_hijos
 * @property string $codigo_empleado
 * @property string $fecha_ingreso
 * @property integer $categoria_ingreso_id
 * @property string $username_corp
 * @property string $password_corp
 * @property string $numero_ivss
 * @property integer $estructura_organizativa_actual_id
 * @property integer $tipo_nomina_id
 * @property integer $condicion_actual_id
 * @property integer $tipo_cargo_actual_id
 * @property integer $cargo_actual_id
 * @property integer $plantel_actual_id
 * @property integer $tipo_serial_cuenta_id
 * @property integer $tipo_cuenta_id
 * @property integer $banco_id
 * @property string $numero_cuenta
 * @property string $origen_titular
 * @property integer $cedula_titular
 * @property string $nombre_titular
 * @property string $resultado_asignacion_ti
 * @property string $fecha_ultima_asistencia_reg
 * @property integer $ultima_asistencia_reg
 * @property string $fecha_egreso
 * @property string $observacion
 * @property string $usuario_ini_id
 * @property string $fecha_ini
 * @property string $usuario_act_id
 * @property string $fecha_act
 * @property string $busqueda
 * @property string $codigo_integridad
 * @property string $lateralidad
 * @property string $estatus
 * @property string $verificado_saime
 * @property string $habilidad_agropecuaria
 * @property integer $banco_tarjeta_alimentacion_id
 * @property integer $numero_tarjeta_alimentacion
 * @property date $fecha_entrega_tarjeta_alimentacion
 * 
 * The followings are the available model relations:
 * @property CategoriaIngreso $categoriaIngreso
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 * @property TipoSerialCuenta $tipoSerialCuenta
 * @property TipoNomina $tipoNomina
 * @property TipoCuenta $tipoCuenta
 * @property TipoCargoNominal $tipoCargoActual
 * @property Plantel $plantelActual
 * @property Parroquia $parroquia
 * @property Municipio $municipio
 * @property Mision $mision
 * @property GradoInstruccion $gradoInstruccion
 * @property EstructuraOrganizativa $estructuraOrganizativaActual
 * @property Estado $estado
 * @property CondicionNominal $condicionActual
 * @property CargoNominal $cargoActual
 * @property Banco $banco
 * @property Banco $bancoAlimentacion
 * @property EstatusEmpleado $estatus
 * @property EstructuraOrganizativa[] $estructuraOrganizativas
 * @property FamiliarEmpleado[] $familiarEmpleados
 * @property IngresoEmpleado[] $ingresoEmpleados
 * @property DocumentoEmpleado[] $documentoEmpleados
 * @property EgresoEmpleado[] $egresoEmpleados
 */
class EgresoMasivoTalentoHumano extends TalentoHumano {
    
    /**
     * Lee el archivo excel y efectúa y llama al proceso que efectúa la carga de Egresos Masivos de Talento Humano
     * 
     * @param array $sheetData
     * @param integer $usuarioId
     * @param string $usuario
     * @param string $ipUsuario
     * @return type
     */
    public function loadRegistroEgresoMasivo($sheetData, $usuarioId, $usuario, $ipUsuario, $modulo) {

        $response = new stdClass();

        $response->data = new stdClass();
        $response->data = array();
        $i = 2; //$i sirve para recorrer el excel
        $j = $i - 2; // $j sirve para crear arreglos de respuesta

        while (array_key_exists($i, $sheetData) && trim($sheetData[$i]['A']) != '') {

            $response->data[$j] = new stdClass();

            //imprimimos el contenido de la celda utilizando la letra de cada columna
            $objCell = $sheetData[$i];

            $origen = strtoupper(utf8_decode(trim($objCell['A'])));
            $cedula = utf8_decode(trim($objCell['B']));
            $estado = strtoupper(utf8_decode(trim($objCell['C'])));
            $motivoEgreso = strtoupper(utf8_decode(trim($objCell['D'])));
            $descripcionEgreso = strtoupper(utf8_decode(trim($objCell['E'])));
            $fechaEgreso = strtoupper(utf8_decode(trim($objCell['F'])));
            $solicitadoPor = strtoupper(utf8_decode(trim($objCell['G'])));
            
            $estadoData = CEstado::getData('nombre', $estado);
            $estadoId = (isset($estadoData[0]))?$estadoData[0]['id']:null;
            
            $motivoEgresoData = CEstado::getData('nombre', $motivoEgreso);
            $motivoEgresoId = ($motivoEgresoData[0])?$motivoEgresoData[0]['id']:null;
            
            $fechaEgresoServer = null;
            
            // Bloque de transformacion y tratamiento de los datos para su mejor procesamiento. Verificar si la fecha compl se va a solicitar, por los momentos no
            if (Helper::isInteger($fechaEgreso)) {
                $UNIX_DATE = ($fechaEgreso - 25569) * 86400;
                $fechaEgreso = gmdate("d-m-Y", $UNIX_DATE);
                $fechaEgresoServer = Helper::toServerDate($fechaEgreso);
            } else {
                $fechaEgreso = $fechaEgreso;
                $fechaEgresoServer = Utiles::toServerDate($fechaEgreso);
            }
            
            // Colocando los Datos leidos de la hoja de cálculo en la respuesta
            $response->data[$j]->cedula = "$origen-$cedula";
            $response->data[$j]->estado = $estado;
            $response->data[$j]->motivoEgreso = $motivoEgreso;
            $response->data[$j]->fechaEgreso = $fechaEgreso;

            //Empezamos con los filtros y validaciones para luego procesar los registros de Egresos de Talento Humano
            if (in_array($origen, array('V', 'E', 'P'))){ // Se valida que los valores contenidos en el campo origen sean válidos
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 10;
                $response->data[$j]->mensaje              = "El Campo 'Origen' debe contener uno de estos dos caracteres V ó E";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!helper::isValidDate($fechaEgresoServer, 'y-m-d')){ // Se valida la fecha de Egreso
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 11;
                $response->data[$j]->mensaje              = "No se ha recibido una Fecha de Egreso en el formato válido (DD/MM/AAAA). Fecha leida del archivo: '.$fechaEgreso.'.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($cedula)){ // Se valida que la Cédula sea un valor numérico
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 12;
                $response->data[$j]->mensaje              = "El Número de Cédula debe contener solo caracteres numéricos";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($estadoId)){ // Se valida que el estado contenga un valor válido
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 13;
                $response->data[$j]->mensaje              = "El Estado Ingresado no es válido. Asegurese de Seleccionar un Estado válido del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($motivoEgresoId)){ // Se valida que el motivo de egreso contenga un valor válido
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 14;
                $response->data[$j]->mensaje              = "El Motivo de Egreso del Talento Humano no es válido. Asegurese de Seleccionar un Motivo de Egreso válido del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(strlen($descripcionEgreso)){ // Se valida la descripción del motivo sea un campo lleno
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 15;
                $response->data[$j]->mensaje              = "La Descripción del Motivo de Egreso del Talento Humano es un campo obligatorio.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(strlen($solicitadoPor)){ // Se valida que el campo solicitado por esté lleno
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 16;
                $response->data[$j]->mensaje              = "Es obligatorio indicar quién ha solicitado el egreso de este talento humano.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } else {
                
                $this->testSpEgresos($origen, $cedula, $estadoId, $motivoEgresoId, $descripcionEgreso, $fechaEgreso, $solicitadoPor, $usuarioId, $usuario, $ipUsuario, $modulo);
                
                $spResponse = $this->spEgreso($origen, $cedula, $estadoId, $motivoEgresoId, $descripcionEgreso, $fechaEgreso, $solicitadoPor, $usuarioId, $usuario, $ipUsuario, $modulo);
                
                if($spResponse['resultado']=='SUCCESS'){
                    $response->data[$j]->proceso_exitoso = 1;
                }
                elseif($spResponse['resultado']=='ERROR'){
                    $response->data[$j]->proceso_exitoso = 0;
                }
                else{
                    $response->data[$j]->proceso_exitoso = 2;
                }
                    
                $response->data[$j]->codigo_resultado = $spResponse['codigo_resultado'];
                $response->data[$j]->mensaje = $spResponse['mensaje'];
                
                $result = ($response->data[$j]->proceso_exitoso)?true:false;

                $response->data[$j]->response = $spResponse;

                if ($response->data[$j]->proceso_exitoso == 1) {
                    $response->data[$j]->resultado = "<span style='color:#33DD00;'>CARGADO</span>";
                } elseif ($response->data[$j]->proceso_exitoso == 0) {
                    $response->data[$j]->resultado = "<span style='color:#FF0000;'>NO CARGADO</span>";
                } else {
                    $response->data[$j]->resultado = "<span style='color:#66AA00;'>CON ADVERTENCIAS</span>";
                }
            }

            $i++;
            $j = $i - 2;
        }

        return array($result, $response->data);
    }

    public function spEgreso($origen, $cedula, $estadoId, $motivoEgresoId, $descripcionEgreso, $fechaEgreso, $solicitadoPor, $usuarioId, $usuario, $ipUsuario, $modulo) {

        $sql = 'SELECT * FROM gestion_humana.ejecucion_egreso_empleado(
                        :ORIGEN_VI::CHARACTER VARYING,
                        :CEDULA_VI::INTEGER,
                        :ESTADO_ID_VI::INTEGER,
                        :MOTIVO_EGRESO_ID_VI::INTEGER,
                        :MOTIVO_DESCRIPCION_VI::TEXT,
                        :FECHA_EGRESO_VI::DATE,
                        :SOLICITADO_POR_VI::CHARACTER VARYING,
                        :MODULO_VI::CHARACTER VARYING,
                        :USUARIO_ID_VI::INTEGER,
                        :USUARIO_VI::CHARACTER VARYING,
                        :DIR_IP_VI::CHARACTER VARYING
                ) AS f(
                    codigo_resultado character varying(15), 
                    resultado character varying(15), 
                    mensaje text
                )';

        //echo "<pre>$sql</pre>";
        //die();
        
        $conection = Yii::app()->db;
        $query = $conection->createCommand($sql);
        
        $query->bindParam(':ORIGEN_VI', $origen, PDO::PARAM_STR);
        $query->bindParam(':CEDULA_VI', $cedula, PDO::PARAM_INT);
        $query->bindParam(':ESTADO_ID_VI', $estadoId, PDO::PARAM_INT);
        $query->bindParam(':MOTIVO_EGRESO_ID_VI', $motivoEgresoId, PDO::PARAM_INT);
        $query->bindParam(':MOTIVO_DESCRIPCION_VI', $descripcionEgreso, PDO::PARAM_STR);
        $query->bindParam(':FECHA_EGRESO_VI', $fechaEgreso, PDO::PARAM_STR);
        $query->bindParam(':SOLICITADO_POR_VI', $solicitadoPor, PDO::PARAM_STR);
        $query->bindParam(':MODULO_VI', $modulo, PDO::PARAM_STR);
        $query->bindParam(':USUARIO_ID_VI', $usuarioId, PDO::PARAM_STR);
        $query->bindParam(':USUARIO_VI', $usuario, PDO::PARAM_STR);
        $query->bindParam(':DIR_IP_VI', $ipUsuario, PDO::PARAM_STR);
        
        $queryResponse = $query->queryRow();

        //echo '<pre>'.$this->getPrintSpEgreso($usuario, $ip_usuario).'</pre>';
        //var_dump($queryResponse);die();

        return $queryResponse;
    }
    
    private function testSpEgresos($origen, $cedula, $estadoId, $motivoEgresoId, $descripcionEgreso, $fechaEgreso, $solicitadoPor, $usuarioId, $usuario, $ipUsuario, $modulo){
        $spTesting = $this->getPrintSpEgreso($origen, $cedula, $estadoId, $motivoEgresoId, $descripcionEgreso, $fechaEgreso, $solicitadoPor, $usuarioId, $usuario, $ipUsuario, $modulo);
        echo "<pre>$spTesting</pre>";
        die();
    }
    

    public function getPrintSpEgreso($origen, $cedula, $estadoId, $motivoEgresoId, $descripcionEgreso, $fechaEgreso, $solicitadoPor, $usuarioId, $usuario, $ipUsuario, $modulo) {

         return "SELECT * FROM gestion_humana.ejecucion_egreso_empleado(
                        '$origen'::CHARACTER VARYING,
                        $cedula::INTEGER,
                        $estadoId::INTEGER,
                        $motivoEgresoId::INTEGER,
                        '$descripcionEgreso'::TEXT,
                        '$fechaEgreso'::DATE,
                        '$solicitadoPor'::CHARACTER VARYING,
                        '$modulo'::CHARACTER VARYING,
                        $usuarioId:USUARIO_ID_VI::INTEGER,
                        '$usuario':USUARIO_VI::CHARACTER VARYING,
                        '$ipUsuario'::CHARACTER VARYING
                ) AS f(
                    codigo_resultado character varying(15), 
                    resultado character varying(15), 
                    mensaje text
                );
              ";
    }

}
