<?php
/**
 * Catalogo de $nombreClass
 *
 * @author Generador de Código
 */
class CCargoNominal extends CCatalogo { 

    protected static $columns = 
        array (
  0 => 'id',
  1 => 'codigo',
  2 => 'nombre',
  3 => 'siglas',
  4 => 'descripcion',
  5 => 'funciones',
  6 => 'tipo_cargo_id',
);

    /**
     * Setea la data en una propiedad static llamada data
     */
    protected static function setData(){

        self::$data = 
        array (
  0 => 
  array (
    'id' => 9,
    'codigo' => 'CN0009',
    'nombre' => 'Adjunto del Gerente General',
    'siglas' => 'AGGCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 5,
  ),
  1 => 
  array (
    'id' => 24,
    'codigo' => 'CN0024',
    'nombre' => 'Bachiller de Consultoría Jurídica',
    'siglas' => 'BCJCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 9,
  ),
  2 => 
  array (
    'id' => 23,
    'codigo' => 'CN0023',
    'nombre' => 'Bachiller de Desarrollo de la Producción y Alimentación Escolar',
    'siglas' => 'BGDPAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 9,
  ),
  3 => 
  array (
    'id' => 25,
    'codigo' => 'CN0025',
    'nombre' => 'Bachiller de Recursos Humanos',
    'siglas' => 'BRRHHCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 9,
  ),
  4 => 
  array (
    'id' => 29,
    'codigo' => 'CN0029',
    'nombre' => 'Cocinero(a) Escolar',
    'siglas' => 'CECNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 11,
  ),
  5 => 
  array (
    'id' => 19,
    'codigo' => 'CN0019',
    'nombre' => 'Coordinador de Diseño Gráfico',
    'siglas' => 'CODGCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 6,
  ),
  6 => 
  array (
    'id' => 8,
    'codigo' => 'CN0008',
    'nombre' => 'Gerente de Administración y Finanzas',
    'siglas' => 'GPFCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 3,
  ),
  7 => 
  array (
    'id' => 5,
    'codigo' => 'CN0005',
    'nombre' => 'Gerente de Comunicaciones y Relaciones Institucionales',
    'siglas' => 'GCRICNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 3,
  ),
  8 => 
  array (
    'id' => 6,
    'codigo' => 'CN0006',
    'nombre' => 'Gerente de Consultoría Jurídica',
    'siglas' => 'GCJCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 3,
  ),
  9 => 
  array (
    'id' => 3,
    'codigo' => 'CN0003',
    'nombre' => 'Gerente de Desarrollo de la Producción de Alimentación Escolar',
    'siglas' => 'GDPACNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 3,
  ),
  10 => 
  array (
    'id' => 7,
    'codigo' => 'CN0007',
    'nombre' => 'Gerente de Planificación y Presupuesto',
    'siglas' => 'GPPCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 3,
  ),
  11 => 
  array (
    'id' => 4,
    'codigo' => 'CN0004',
    'nombre' => 'Gerente de RRHH',
    'siglas' => 'GRRHHCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 3,
  ),
  12 => 
  array (
    'id' => 26,
    'codigo' => 'CN0026',
    'nombre' => 'Gerente de Tecnología e Información',
    'siglas' => 'GTICNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 3,
  ),
  13 => 
  array (
    'id' => 2,
    'codigo' => 'CN0002',
    'nombre' => 'Gerente General',
    'siglas' => 'GGCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 2,
  ),
  14 => 
  array (
    'id' => 1,
    'codigo' => 'CN0001',
    'nombre' => 'Presidente de la Corporación',
    'siglas' => 'PCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 1,
  ),
  15 => 
  array (
    'id' => 22,
    'codigo' => 'CN0022',
    'nombre' => 'Profesional de Atención al Ciudadano',
    'siglas' => 'PACCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 7,
  ),
  16 => 
  array (
    'id' => 21,
    'codigo' => 'CN0021',
    'nombre' => 'Profesional de Consultoría Jurídica',
    'siglas' => 'PCJCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 7,
  ),
  17 => 
  array (
    'id' => 20,
    'codigo' => 'CN0020',
    'nombre' => 'Profesional de Recursos Humanos',
    'siglas' => 'PRRHHCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 7,
  ),
  18 => 
  array (
    'id' => 10,
    'codigo' => 'CN0010',
    'nombre' => 'Sub-Gerente de Administración',
    'siglas' => 'SGACNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  19 => 
  array (
    'id' => 16,
    'codigo' => 'CN0016',
    'nombre' => 'Sub-Gerente de Captación',
    'siglas' => 'SGCCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  20 => 
  array (
    'id' => 27,
    'codigo' => 'CN0027',
    'nombre' => 'Sub-Gerente de Desarrollo de Aplicaciones',
    'siglas' => 'SGDACNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  21 => 
  array (
    'id' => 18,
    'codigo' => 'CN0018',
    'nombre' => 'Sub-Gerente de Estrategia Comunicacional',
    'siglas' => 'SGECCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  22 => 
  array (
    'id' => 15,
    'codigo' => 'CN0015',
    'nombre' => 'Sub-Gerente de Finanzas',
    'siglas' => 'SGFCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  23 => 
  array (
    'id' => 13,
    'codigo' => 'CN0013',
    'nombre' => 'Sub-Gerente de Nómina',
    'siglas' => 'SGNCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  24 => 
  array (
    'id' => 11,
    'codigo' => 'CN0011',
    'nombre' => 'Sub-Gerente de Presupuesto',
    'siglas' => 'SGPCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  25 => 
  array (
    'id' => 14,
    'codigo' => 'CN0014',
    'nombre' => 'Sub-Gerente de Producción Agricola Escolar',
    'siglas' => 'SGPAECNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  26 => 
  array (
    'id' => 28,
    'codigo' => 'CN0028',
    'nombre' => 'Sub-Gerente de Redes e Infraestructura',
    'siglas' => 'SGRICNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  27 => 
  array (
    'id' => 17,
    'codigo' => 'CN0017',
    'nombre' => 'Sub-Gerente de Relaciones Institucionales',
    'siglas' => 'SGRICNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
  28 => 
  array (
    'id' => 12,
    'codigo' => 'CN0012',
    'nombre' => 'Sub-Gerente de Soporte Técnico',
    'siglas' => 'SGSTCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'tipo_cargo_id' => 4,
  ),
)		; 

    	}
}
