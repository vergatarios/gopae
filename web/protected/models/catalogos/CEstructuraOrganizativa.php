<?php
/**
 * Catalogo de $nombreClass
 *
 * @author Generador de Código
 */
class CEstructuraOrganizativa extends CCatalogo { 

    protected static $columns = 
        array (
  0 => 'id',
  1 => 'codigo',
  2 => 'nombre',
  3 => 'siglas',
  4 => 'descripcion',
  5 => 'empleados_necesarios',
);

    /**
     * Setea la data en una propiedad static llamada data
     */
    protected static function setData(){

        self::$data = 
        array (
  0 => 
  array (
    'id' => 5,
    'codigo' => 'A0005',
    'nombre' => 'Auditoría Interna',
    'siglas' => 'AI',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  1 => 
  array (
    'id' => 3,
    'codigo' => 'A0003',
    'nombre' => 'Consultoría Jurídica',
    'siglas' => 'CJ',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  2 => 
  array (
    'id' => 39,
    'codigo' => 'E0039',
    'nombre' => 'Coordinación de Diseño Gráfico',
    'siglas' => 'CDG',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  3 => 
  array (
    'id' => 6,
    'codigo' => 'A0006',
    'nombre' => 'Gerencia de Administración y Finanzas',
    'siglas' => 'AF',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  4 => 
  array (
    'id' => 9,
    'codigo' => 'A0009',
    'nombre' => 'Gerencia de Comunicaciones y Relaciones Institucionales',
    'siglas' => 'CRI',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  5 => 
  array (
    'id' => 27,
    'codigo' => 'C0027',
    'nombre' => 'Gerencia de Control e Inspección Regional',
    'siglas' => 'GCIR',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  6 => 
  array (
    'id' => 26,
    'codigo' => 'C0026',
    'nombre' => 'Gerencia de Investigación y Formación Nutricional',
    'siglas' => 'GIFN',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  7 => 
  array (
    'id' => 7,
    'codigo' => 'A0007',
    'nombre' => 'Gerencia de Planificación y Presupuesto',
    'siglas' => 'PP',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  8 => 
  array (
    'id' => 25,
    'codigo' => 'C0025',
    'nombre' => 'Gerencia de Procesos Operativos',
    'siglas' => 'GPO',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  9 => 
  array (
    'id' => 8,
    'codigo' => 'A0008',
    'nombre' => 'Gerencia de Tecnología e Información',
    'siglas' => 'TI',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  10 => 
  array (
    'id' => 2,
    'codigo' => 'A0002',
    'nombre' => 'Gerencia General',
    'siglas' => 'GG',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  11 => 
  array (
    'id' => 24,
    'codigo' => 'C0024',
    'nombre' => 'Gerencia para el Desarrollo de la Producción y Alimentación Escolar',
    'siglas' => 'GDPAE',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  12 => 
  array (
    'id' => 10,
    'codigo' => 'A0010',
    'nombre' => 'Oficina de Atención al Ciudadano',
    'siglas' => 'AC',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  13 => 
  array (
    'id' => 4,
    'codigo' => 'A0004',
    'nombre' => 'Oficina de Recursos Humanos',
    'siglas' => 'RH',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  14 => 
  array (
    'id' => 1,
    'codigo' => 'P0001',
    'nombre' => 'Presidencia',
    'siglas' => 'PS',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  15 => 
  array (
    'id' => 14,
    'codigo' => 'B0014',
    'nombre' => 'Subgerencia de Administración',
    'siglas' => 'SAD',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  16 => 
  array (
    'id' => 13,
    'codigo' => 'B0013',
    'nombre' => 'Subgerencia de Bienestar Social',
    'siglas' => 'SBS',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  17 => 
  array (
    'id' => 11,
    'codigo' => 'B0011',
    'nombre' => 'Subgerencia de Capacitación y Desarrollo',
    'siglas' => 'SCD',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  18 => 
  array (
    'id' => 20,
    'codigo' => 'B0020',
    'nombre' => 'Subgerencia de Desarrollo de Aplicaciones',
    'siglas' => 'SDA',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  19 => 
  array (
    'id' => 31,
    'codigo' => 'D0031',
    'nombre' => 'Subgerencia de Distribución y Comercialización',
    'siglas' => 'SDC',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  20 => 
  array (
    'id' => 29,
    'codigo' => 'D0029',
    'nombre' => 'Subgerencia de Encadenamiento Productivo',
    'siglas' => 'SEP',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  21 => 
  array (
    'id' => 22,
    'codigo' => 'B0022',
    'nombre' => 'Subgerencia de Estrategia Comunicacional',
    'siglas' => 'SEC',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  22 => 
  array (
    'id' => 15,
    'codigo' => 'B0015',
    'nombre' => 'Subgerencia de Finanzas',
    'siglas' => 'SFN',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  23 => 
  array (
    'id' => 36,
    'codigo' => 'D0036',
    'nombre' => 'Subgerencia de Inspección Regional',
    'siglas' => 'SIR',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  24 => 
  array (
    'id' => 33,
    'codigo' => 'D0033',
    'nombre' => 'Subgerencia de Investigación Nutricional',
    'siglas' => 'SIN',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  25 => 
  array (
    'id' => 12,
    'codigo' => 'B0012',
    'nombre' => 'Subgerencia de Nómina',
    'siglas' => 'SNM',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  26 => 
  array (
    'id' => 35,
    'codigo' => 'D0035',
    'nombre' => 'Subgerencia de Normalización',
    'siglas' => 'SNCIR',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  27 => 
  array (
    'id' => 30,
    'codigo' => 'D0030',
    'nombre' => 'Subgerencia de Planificación de Adquisiciones y Almacenamiento',
    'siglas' => 'SPAA',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  28 => 
  array (
    'id' => 32,
    'codigo' => 'D0032',
    'nombre' => 'Subgerencia de Planificación y Contro Operativo',
    'siglas' => 'SPC',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  29 => 
  array (
    'id' => 17,
    'codigo' => 'B0017',
    'nombre' => 'Subgerencia de Planificación y Organización',
    'siglas' => 'SPO',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  30 => 
  array (
    'id' => 18,
    'codigo' => 'B0018',
    'nombre' => 'Subgerencia de Presupuesto',
    'siglas' => 'SPR',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  31 => 
  array (
    'id' => 28,
    'codigo' => 'D0028',
    'nombre' => 'Subgerencia de Producción Agricola Escolar',
    'siglas' => 'SPAE',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  32 => 
  array (
    'id' => 34,
    'codigo' => 'D0034',
    'nombre' => 'Subgerencia de Programas de Formación Nutricional',
    'siglas' => 'SPFN',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  33 => 
  array (
    'id' => 21,
    'codigo' => 'B0021',
    'nombre' => 'Subgerencia de Redes e Infraestructura',
    'siglas' => 'SRI',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  34 => 
  array (
    'id' => 23,
    'codigo' => 'B0023',
    'nombre' => 'Subgerencia de Relaciones Institucionales',
    'siglas' => 'SRI',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  35 => 
  array (
    'id' => 16,
    'codigo' => 'B0016',
    'nombre' => 'Subgerencia de Servicios Generales',
    'siglas' => 'SSG',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  36 => 
  array (
    'id' => 19,
    'codigo' => 'B0019',
    'nombre' => 'Subgerencia de Soporte Técnico',
    'siglas' => 'SST',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  37 => 
  array (
    'id' => 37,
    'codigo' => 'B0037',
    'nombre' => 'Unidad de Control Posterior',
    'siglas' => 'UCP',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
  38 => 
  array (
    'id' => 38,
    'codigo' => 'B0038',
    'nombre' => 'Unidad de Determinación de Responsabilidades',
    'siglas' => 'UDR',
    'descripcion' => NULL,
    'empleados_necesarios' => NULL,
  ),
)		; 

    	}
}
