<?php

class ReporteRegistroUnicoDiario extends CActiveRecord {

    
    public $cacheIndexRegUnico = 'A-REGISTRO-UNICO-AUTOMATICO';
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'rep_reg_unic.estadistico';
    }
    
    /**
     * @author Jose Gabriel Gonzalez y Nelson Gonzalez
     * Este metodo permite obtener el reporte estadistico del registro unico del CNAE
     * 
     * @param string $level
     * @param integer $dependency_id
     * @return array
     */
    
    
    public function reporteEstadisticoRegistroUnicoDiario($fecha, $hora, $accion) {
        $sql = "SELECT * FROM rep_reg_unic.reporte_estadistico_registro_unico_diario(:fecha, :hora, :accion) AS f(a text)";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $command->bindParam(":fecha", $fecha, PDO::PARAM_STR);
        $command->bindParam(":hora", $hora, PDO::PARAM_STR);
        $command->bindParam(":accion", $accion, PDO::PARAM_STR);
        $resultado = $command->execute();
    }
    
    
    public function reporteEstadisticoRegistroUnico($level, $dependency_id=null){

       $cacheIndex = $this->cacheIndexRegUnico.'A';
        $resultado = Yii::app()->cache->get($cacheIndex);
        
        if(!$resultado){
            $resultado = array();
            //$orderBy = 'titulo ASC, nombre ASC';
                
            if(in_array($level,array('region', 'estado', 'municipio')) && (is_null($dependency_id) || is_numeric($dependency_id))){
                if($level=='estado'){
                    $camposSeleccionados = "e.region_id AS region_id, r.nombre AS region, e.id AS id, e.nombre, 'AAA'||e.nombre AS titulo  ";
                    $camposSeleccionadosTotales = "$dependency_id AS region_id, 'X' AS region, 0 AS id, 'TOTAL' AS nombre , 'ZZZTOTAL' AS titulo";
                    $camposAgrupados = "e.region_id, r.nombre, e.id, e.nombre, titulo  ";
                    $where = 'e.id != 45 AND e.region_id = '.$dependency_id; // Excluye Dependencias Federales (Id=45)
                    $orderBy = 'titulo ASC, nombre ASC';
            }
            $sql = "SELECT 
                        region_id, region, estado_id as id, estado_nombre as nombre, titulo, 
                        planteles,
                        total_cnae,
                        autoridades_verificados,
                        autoridades_verificados_sin_foto,
                        cocineras_escolares,
                        cocineras_escolares_asignadas,
                        beneficiados_x_mercal,
                        beneficiados_x_pdval,
                        beneficiados_x_otros,
                        fecha,
                        hora
                    FROM 
                        rep_reg_unic.estadistico 
                    ORDER BY
                        titulo ASC, nombre ASC";

                //echo "<pre><code>$sql</code></pre>"; die();
                $connection = Yii::app()->db;
                $command = $connection->createCommand($sql);
                $resultado = $command->queryAll();
                
                if(is_array($resultado) && count($resultado)>0){
                    Yii::app()->cache->set($cacheIndex, $resultado, 3600);
                }
            }
        }
     
        return $resultado;
    }
    
    public function fechaCompletaLog(){

        $cacheIndex = $this->cacheIndexRegUnico.'FECHA-';
        $resultado = Yii::app()->cache->get($cacheIndex);
        
                
        $sql = "SELECT 
                    hora,
                    fecha
                FROM 
                    rep_reg_unic.log_estado 
                WHERE
                    estatus = 'A'";

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $resultado = $command->queryRow();

        
        return $resultado;
    }
    public  function detalleRegistroUnicoPlanteles($columna,$estadoId,$filas=null){
        $resultado=array();
        $filtroEstado='';
        $filtroColumna='';
        $limiteFilas='';

        if(in_array($columna,array('planteles','total_cnae', 'beneficiados_x_mercal', 'beneficiados_x_pdval', 'beneficiados_x_otros',)) and is_numeric($estadoId) AND (is_null($filas) OR is_numeric($filas))){
            if($estadoId!='0'){
                $filtroEstado=' AND estado_id='.$estadoId;
            }

            if($columna=='planteles'){
                $filtroColumna=' AND plantel_id IS NOT NULL ';
            }elseif($columna=='total_cnae'){
                $filtroColumna=" AND cod_cnae IS NOT NULL AND es_beneficiario_pae = 'SI' AND matricula_general>0 ";
            }elseif($columna=='beneficiados_x_mercal'){
                $filtroColumna=" AND cod_cnae IS NOT NULL AND proveedor_actual_id = 5 ";
            }elseif($columna=='beneficiados_x_pdval'){
                $filtroColumna=" AND cod_cnae IS NOT NULL AND proveedor_actual_id = 6 ";
            }elseif($columna=='beneficiados_x_otros'){
                $filtroColumna=" AND cod_cnae IS NOT NULL AND proveedor_actual_id != 6 AND proveedor_actual_id != 5 ";
            }

            if(is_numeric($filas) AND (int)$filas>=0){
                $limiteFilas = ' LIMIT '.(int)$filas;
            }

            $sql="
            SELECT 
                cod_plantel,
                cod_estadistico,
                nombre_plantel,
                annio_fundado,
                cod_cnae,
                codigo_ner,
                registro_cnae,
                denominacion,
                dependencia,
                estado,
                estado_capital,
                municipio,
                municipio_capital,
                zona_educativa,
                pae_activo,
                tipo_servicio_pae,
                ingestas,
                cantidad_madres_procesadoras,
                matricula_maternal,
                matricula_preescolar,
                matricula_educacion_primaria,
                matricula_educacion_media_general,
                matricula_educacion_tecnica,
                origen_director,
                cedula_director,
                apellido_director,
                nombre_director,
                telefono_director,
                email_director,
                origen_enlace_pae,
                cedula_enlace_pae,
                apellido_enlace_cnae,
                nombre_enlace_cnae,
                telefono_enlace_cnae,
                email_enlace_cnae
            FROM 
                rep_reg_unic.detalle_plantel
            WHERE 1=1 $filtroEstado $filtroColumna $limiteFilas";
            
            // echo "<pre><code>$sql</code></pre>"; die();
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $resultado = $command->queryAll();

        }

        return $resultado;
    }
    
    public  function detalleRegistroUnicoAutoridades($columna,$estadoId,$filas=null){
        $resultado=array();
        $filtroEstado='';
        $filtroColumna='';
        $limiteFilas='';
        if(in_array($columna,array('autoridades_verificados','autoridades_verificados_sin_foto')) and is_numeric($estadoId) AND (is_null($filas) OR is_numeric($filas))){
            if($estadoId!='0'){
                $filtroEstado=" AND estado_id = $estadoId";
            }

            if($columna=='autoridades_verificados'){
                //echo "<pre><code>$sql</code></pre>"; die();
                $filtroColumna=" AND es_beneficiario_pae = 'SI' AND presento_documento_identidad = 'SI' AND foto IS NOT NULL AND (director_actual_id IS NOT NULL OR subdirector_actual_id IS NOT NULL OR enlace_pae_actual_id IS NOT NULL) AND estatus = 'A' AND cargo_id IN (3, 5, 27)";
            }elseif($columna=='autoridades_verificados_sin_foto'){
                //echo "<pre><code>$sql</code></pre>"; die();
                $filtroColumna=" AND es_beneficiario_pae = 'SI' AND presento_documento_identidad = 'SI' AND (foto IS NULL OR foto='') AND (director_actual_id IS NOT NULL OR subdirector_actual_id IS NOT NULL OR enlace_pae_actual_id IS NOT NULL) AND estatus = 'A' AND cargo_id IN (3, 5, 27)";
            }

            if(is_numeric($filas) AND (int)$filas>=0){
                //$limiteFilas = ' LIMIT '.(int)$filas;
            }

            $sql="
             SELECT
                cod_plantel,
                cod_estadistico,
                nombre_plantel,
                annio_fundado,
                cod_cnae,
                codigo_ner,
                registro_cnae,
                denominacion,
                dependencia,
                estado,
                estado_capital,
                municipio,
                municipio_capital,
                zona_educativa,
                pae_activo,
                tipo_servicio_pae,
                ingestas,
                cantidad_madres_procesadoras,
                matricula_maternal,
                matricula_preescolar,
                matricula_educacion_primaria,
                matricula_educacion_media_general,
                matricula_educacion_tecnica,
                responsabilidad_autoridad,
                origen_autoridad,
                cedula_autoridad,
                apellido_autoridad,
                nombre_autoridad,
                telefono_autoridad,
                email_autoridad,
                foto,
                es_beneficiario_pae,
                presento_documento_identidad,
                director_actual_id,
                subdirector_actual_id,
                enlace_pae_actual_id,
                estatus,
                cargo_id
            FROM 
                rep_reg_unic.detalle_autoridad 
            WHERE 1=1 $filtroEstado $filtroColumna $limiteFilas";

            // echo "<pre><code>$sql</code></pre>"; die();
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $resultado = $command->queryAll();

        }

        return $resultado;
    }
    
    public  function detalleRegistroUnicoCocineras($columna,$estadoId,$filas=null){
        $resultado=array();
        $filtroEstado='';
        $filtroColumna='';
        $limiteFilas='';
        if(in_array($columna,array("cocineras_escolares", "cocineras_escolares_asignadas")) and is_numeric($estadoId) AND (is_null($filas) OR is_numeric($filas))){
            if($estadoId!='0'){
                $filtroEstado=' AND estado_id='.$estadoId;
            }
            
            if($columna=='cocineras_escolares'){
                $filtroColumna=" AND estatus = 'E' AND tipo_cargo_actual_id = 11 ";
            }elseif($columna=='cocineras_escolares_asignadas'){
                $filtroColumna=" AND plantel_actual_id IS NOT NULL AND estatus = 'E' AND tipo_cargo_actual_id = 11 ";
            }

            if(is_numeric($filas) AND (int)$filas>=0){
                $limiteFilas = ' LIMIT '.(int)$filas;
            }

            $sql="
            SELECT 
                estado_cocinera_escolar,
                origen,
                cedula,
                nombre,
                apellido,
                fecha_nacimiento,
                sexo,
                telefono_celular,
                telefono_fijo,
                email_personal,
                cod_plantel,
                cod_estadistico,
                nombre_plantel,
                annio_fundado,
                cod_cnae,
                codigo_ner,
                registro_cnae,
                denominacion,
                dependencia,
                estado,
                estado_capital,
                municipio,
                origen_director,
                cedula_director,
                apellido_director,
                nombre_director,
                telefono_director,
                email_director,
                estatus,
                tipo_cargo_actual_id,
                plantel_actual_id
            FROM rep_reg_unic.detalle_cocinera 
            WHERE 1=1 $filtroEstado $filtroColumna $limiteFilas";
            
             //echo "<pre><code>$sql</code></pre>"; die();
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $resultado = $command->queryAll();

        }

        return $resultado;
    }
}


