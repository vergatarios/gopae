-- Function: gplantel.validate_asignacion_madre_cocinera(character varying, integer, integer, character varying, integer, character varying, character varying)

-- DROP FUNCTION gplantel.validate_asignacion_madre_cocinera(character varying, integer, integer, character varying, integer, character varying, character varying);

CREATE OR REPLACE FUNCTION gestion_humana.ejecucion_egreso_empleado(
    origen_vi character varying,
    cedula_vi integer,
    estado_id_vi integer,
    motivo_egreso_id_vi integer,
    motivo_descripcion_vi text,
    fecha_egreso_vi date,
    solicitado_por_vi character varying,
    modulo_vi character varying,
    usuario_vi integer,
    username_vi character varying,
    ipaddress_vi character varying)
  RETURNS record AS
$BODY$
DECLARE

    -- Datos del Talento Humano
    cant_talento_humano_v INTEGER := 0;
    talento_humano_id_v INTEGER;
    estatus_talento_humano_v CHARACTER VARYING(1);
    tipo_cargo_id_talento_humano_v INTEGER;
    fecha_ingreso_talento_humano_v DATE;
    fecha_egreso_talento_humano_v DATE;
    plantel_actual_id_v INTEGER;
    estado_talento_humano_id_v INTEGER;

    -- Datos de Cocinera(o) Escolar
    cant_cocinera_escolar_v INTEGER := 0;

    -- Datos de Ingreso
    cant_ingreso_v INTEGER := 0;
    ingreso_id_v INTEGER;

    -- Datos de Egreso
    egreso_id_v INTEGER;

    obrero_id_v INTEGER := 11;
    estatus_empleado_v CHARACTER VARYING(1) := 'E';
    fecha_minima_v DATE := '2014-09-01'::DATE;

    ente_v CHARACTER VARYING(50) := 'la Corporación Nacional de Alimentación Escolar';

    codigo_integridad_v CHARACTER VARYING(50) := '';

    -- Auditoria
    fecha_v TIMESTAMP WITHOUT TIME ZONE; -- Fecha Actual.

    -- Resultado
    codigo_v CHARACTER VARYING(15); -- Código del Resultado de la transaccion. S0000...
    transaccion_v CHARACTER VARYING(15); -- Resultado de la transaccion en una palabra o expresion clave. EXITO, ALERTA, ERROR
    mensaje_v CHARACTER VARYING(300); -- Mensaje que podría ser mostrado al usuario para indicarle el resultado de la transaccion.
    resultado RECORD; -- Sera el Resultado que devolvera la funcion.

    -- Debug
    seccion SMALLINT := 1;

BEGIN

    -- ESTA FUNCION PERMITE REALIZAR EL PROCESO DE EGRESO DE UN EMPLEADO, SOLO EL PROCESO DE DESVINCULACIÓN (SIN CALCULOS DE LIQUIDACIONES Y OTROS PAGOS).
    -- @author José Gabriel González
    -- @date 2015-03-30 18:36

    -- TABLA DE RESPUESTA QUE PODRÁ DAR LA FUNCION PL/PGSQL
    -- ------------------------------------------------------------------------------------------------------------------
    --  CODIGO | RESULTADO | MENSAJE
    -- ------------------------------------------------------------------------------------------------------------------
    --  S0000  | EXITO     | X El Proceso de Egreso se ha completado exitósamente.
    --  E0001  | ERROR     | X El Talento Humano con la Cédula de Identidad Indicada no se encuentra registrado en el Sistema.
    --  E0002  | ERROR     | X La Persona posee un estatus de empleado que indica que no pertenece a la corporación.
    --  E0003  | ERROR     | X La Persona no posee registro de ingreso.
    --  E0004  | ERROR     | X La Persona no pertenece al Estado Indicado.
    --  E0005  | ERROR     | X .
    --  E0006  | ERROR     | X .
    --  E0007  | ERROR     | X .
    --  E0008  | ERROR     | X .
    -- ------------------------------------------------------------------------------------------------------------------

    RAISE NOTICE 'EGRESO DE TH CON CEDULA: %-%. FECHA: %', origen_vi, cedula_vi, fecha_egreso_vi;

    RAISE NOTICE 'INICIO: Proceso de Egreso de Personal ha Comenzado';

    fecha_v := (now())::timestamp(0);

    seccion := 2;

    SELECT COUNT(1) INTO cant_talento_humano_v FROM gestion_humana.talento_humano th WHERE th.origen = origen_vi AND th.cedula = cedula_vi;

    IF cant_talento_humano_v>0 THEN -- EXISTE LA CÉDULA DE IDENTIDAD DEL TALENTO HUMANO

        seccion := 4;

        SELECT th.id, th.estatus, th.tipo_cargo_actual_id, th.fecha_ingreso, th.fecha_egreso, th.plantel_actual_id, th.estado_id INTO talento_humano_id_v, estatus_talento_humano_v, tipo_cargo_id_talento_humano_v, fecha_ingreso_talento_humano_v, fecha_egreso_talento_humano_v, plantel_actual_id_v, estado_talento_humano_id_v FROM gestion_humana.talento_humano th WHERE th.origen = origen_vi AND th.cedula = cedula_vi LIMIT 1;

        IF estatus_talento_humano_v NOT IN ('A', 'F') THEN -- EL ESTATUS DEL EMPLEADO INDICA QUE AÚN PERTENECE A LA CORPORACIÓN, VER TABLA "gestion_humana.estatus_empleado"

            seccion := 5;

            SELECT COUNT(1) INTO cant_ingreso_v FROM gestion_humana.ingreso_empleado WHERE talento_humano_id = talento_humano_id_v;

            IF cant_ingreso_v>0 THEN -- ESTA PERSONA POSEE UN REGISTRO DE INGRESO A LA CORPORACIÓN
                
                IF estado_talento_humano_id_v == estado_id_vi THEN -- EL ESTADO INDICADO NO CORRESPONDE

                    seccion := 6;

                    SELECT id INTO ingreso_id_v FROM gestion_humana.ingreso_empleado WHERE talento_humano_id = talento_humano_id_v ORDER BY fecha_ingreso DESC LIMIT 1;

                    seccion := 7;

                    -- REGISTRO EL ACTO DE EGRESO DE LA CORPORACIÓN
                    INSERT INTO gestion_humana.egreso_empleado(
                           fecha_egreso,
                           talento_humano_id,
                           ingreso_id,
                           motivo_egreso_id,
                           motivo_descripcion,
                           solicitado_por,
                           usuario_ini_id,
                           fecha_ini,
                           usuario_act_id,
                           fecha_act,
                           estatus
                        )
                    VALUES (
                           fecha_egreso_vi::DATE,
                           talento_humano_id_v,
                           ingreso_id_v,
                           motivo_egreso_id_vi,
                           motivo_descripcion_vi,
                           solicitado_por_vi,
                           usuario_vi,
                           fecha_v,
                           usuario_vi,
                           fecha_v,
                           estatus
                        ) RETURNING id INTO egreso_id_v;

                    seccion := 8;

                    -- CREO EL CODIGO DE INTEGRIDAD DEL REGISTRO PARA ALMACENARLO EN TABLA DE TALENTO HUMANO
                    codigo_integridad_v := md5(talento_humano_id_v::TEXT||fecha_ingreso_talento_humano_v::TEXT||ingreso_id_v::TEXT||fecha_egreso_vi::TEXT||egreso_id_v::TEXT||estatus_talento_humano_v::TEXT||'F'||tipo_cargo_id_talento_humano_v::TEXT||plantel_actual_id_v::TEXT||fecha_v::TEXT||usuario_vi::TEXT);

                    seccion := 9;

                    -- ACTUALIZO LOS DATOS PARA DEJAR CONSTANCIA DEL EGRESO EN LA TABLA DE TALENTO HUMANO DESVINCULANDOLE DEL PLANTEL ACTUAL Y CAMBIANDO SU ESTATUS
                    UPDATE gestion_humana.talento_humano SET fecha_egreso = fecha_egreso_vi::DATE, estatus = 'F', plantel_actual_id = NULL, codigo_integridad = codigo_integridad_v, fecha_act = fecha_v, usuario_act_id = usuario_vi WHERE id = talento_humano_id_v;

                    IF plantel_actual_id_v IS NOT NULL AND tipo_cargo_id_talento_humano_v = obrero_id_v THEN

                        seccion := 10;

                        -- SI ES UNA COCINERA O COCINERO ESCOLAR LO DESVINCULO DEL PLANTEL DONDE PERTENECE ACTUALMENTE
                        UPDATE gplantel.cocinera_plantel SET estatus = 'E', usuario_act_id = usuario_vi, fecha_act = fecha_v, fecha_elim = fecha_v WHERE talento_humano_id = talento_humano_id_v AND estatus = 'A';

                    END IF;

                    codigo_v := 'S0000';
                    transaccion_v := 'EXITO';
                    mensaje_v := 'El Proceso de Egreso se ha efectuado de forma exitosa. La Persona con la Cédula de Identidad Indicada ('||origen_vi||'-'||cedula_vi::TEXT||') ha sido egresada de '||ente_v||'.';
                
                ELSE -- LA PERSONA NO PERTENECE AL ESTADO INDICADO

                    codigo_v := 'E0004';
                    transaccion_v := 'ERROR';
                    mensaje_v := 'La Persona con la Cédula de Identidad Indicada ('||origen_vi||'-'||cedula_vi::TEXT||') No Pertenece al Estado Indicado, verifique la información e intentelo de nuevo.';

                END IF;

            ELSE -- LA PERSONA NO POSEE REGISTRO DE INGRESO

                codigo_v := 'E0003';
                transaccion_v := 'ERROR';
                mensaje_v := 'La Persona con la Cédula de Identidad Indicada ('||origen_vi||'-'||cedula_vi::TEXT||') no posee Registro de Ingreso a '||ente_v||'.';

            END IF;

        ELSE -- ESTATUS DEL EMPLEADO INDICA QUE NO PERTENECE A LA CORPORACIÓN

            codigo_v := 'E0002';
            transaccion_v := 'ERROR';
            mensaje_v := 'La Persona con la Cédula de Identidad Indicada ('||origen_vi||'-'||cedula_vi::TEXT||') no pertenece en estos momentos a '||ente_v||'.';

            IF fecha_egreso_talento_humano_v IS NOT NULL THEN
                mensaje_v := mensaje_v || ' Posee la Fecha de Egreso: '||TO_CHAR(fecha_egreso_talento_humano_v, 'DD-MM-YYYY')||'.';
            ELSIF fecha_ingreso_talento_humano_v IS NULL THEN
                mensaje_v := mensaje_v || ' Esta persona no ha pasado por el proceso de ingreso.';
            END IF;

        END IF;

    ELSE -- CEDULA DE IDENTIDAD NO ENCONTRADA EN EL REGISTRO DEL TALENTO HUMANO DE LA CORPORACIÓN

        seccion := 3;
        codigo_v := 'E0001';
        transaccion_v := 'ERROR';
        mensaje_v := 'La Cédula de Identidad Indicada ('||origen_vi||'-'||cedula_vi::TEXT||') no se encuentra vinculada a un empleado de '||ente_v||'.';

    END IF;

    -- INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username) VALUES (fecha_v, ipaddress_vi, 'ESCRITURA', modulo_vi, transaccion_v, 'REGISTRO INICIAL DE REGISTRO: '||mensaje_v, usuario_vi, username_vi);

    RAISE NOTICE 'FIN: Proceso de Egreso de Empleado';

    INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username) VALUES (fecha_v, ipaddress_vi, 'ESCRITURA', modulo_vi, transaccion_v, 'REGISTRO EGRESOS: '||mensaje_v, usuario_vi, username_vi);

    SELECT codigo_v, transaccion_v, mensaje_v INTO resultado;

    RETURN resultado;

EXCEPTION

    WHEN OTHERS THEN
        codigo_v := SQLSTATE||'';
        transaccion_v := 'ERROR';
        mensaje_v := 'Ha ocurrido un error '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (SECCION: '||seccion||')';
        INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username) VALUES (fecha_v, ipaddress_vi, 'ESCRITURA', modulo_vi, 'ERROR', 'REGISTRO EGRESOS: '||mensaje_v, usuario_vi, username_vi);
        SELECT codigo_v, transaccion_v, mensaje_v INTO resultado;
        RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %) (SECCION: %)', SQLERRM, SQLSTATE, seccion;
    RETURN resultado;

END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
