<?php

    /**
     * Este "Console Aplication" permite generar y/o enviar los Comprobantes del CNAE a sus corespondientes correos de forma masiva.
     * 
     * El siguiente codigo fue tomado del Console Aplication "CorreoComprobanteCnaeCommand" y fue estructurado de forma que funcione para envios masivos
     * @author Daniel Ruiz <darken212@gmail.com>
     * @createAt 2015-03-16
     * @editedAt 2015-03-26
     * 
     **/

class CorreoMasivoComprobanteCnaeCommand extends CConsoleCommand {

    
    //MODULO DEL CONSOLE APLICATION
    private $module = 'registroUnico';

    
    //ACCION PARA MANDAR LOS CORREOS
    //COMANDOS:
    //*  cd /var/www/gopae/web/protected
    //*  php yiic correoMasivoComprobanteCnae envio
    public function actionEnvio() {

        echo Yii::app()->params['adminEmailSend']."\n";

        try {

            $fechaInicio = date('Y-m-d H:i:s');
            echo "\n------------------------------------------------------------------------\n";
            echo "\n----------------------------I  N  I C  I  O-----------------------------\n";
            echo "\n------------------------------------------------------------------------\n";
            echo "$fechaInicio: INICIO DEL PROCESO DE ENVÍO DE CORREO DEL COMPROBANTE CNAE. \n";

            //CAPTURA A TODOS LOS PLANTELES QUE SEAN BENEFICIARIOS DEL PAE
            $plantelIdBeneficiarioPae = Plantel::getPlantelesIdBeneficiariosPae();
            
            
            echo "LA CANTIDAD DE COMPROBANTES BENEFICIARIOS PAE SON <<".count($plantelIdBeneficiarioPae).">>\n";
             
            
                    
                    
                    $mailHost = Yii::app()->params['mailServer'];
                    $mailPort = Yii::app()->params['portMailServer'];
                    echo "\n------------------------------------------------------------------------\n";
                    echo("Se enviará desde el Servidor <<$mailHost:$mailPort>>.\n");

                    $directory = str_replace('//', '/', Yii::app()->params['webDirectoryPath'].Yii::app()->params['urlDownloadComprobanteCnae']);
                    
                    $administrador['nombre'] = "Administrador Gopae";
                    $administrador['correo'] = Yii::app()->params['adminEmailSend'];
                    
                    $contador = 1;
                    
                    //EL foreach TRAE TODOS LOS PLANTELES QUE SON BENEFICIARIOS PAE
                    foreach ($plantelIdBeneficiarioPae as $comprobante) {
                        
                        echo "\n------------------------------------------------------------------------\n";
                        echo "\n------------------------------------------------------------------------\n";
                        
                       //LUEGO DE ENVIAR 100 CORREOS, ESPERA 90 SEGUNDOS
                        if($contador==100){
                            sleep(90);
                            $contador = 1;
                        }

                        //ES NECESARIO QUE EL ARRAY DEL SQL DE PLANETELES QUE SON BENEFICIARIOS PAE GENERE LA DATA DESDE UN MODELO
                        $idPlantel = $comprobante['id'];
                        
                        $model = Plantel::model()->find(array('condition' => 't.id = :id', 'params'=>array('id'=>$idPlantel)));

                        //MODULO DEL COMPROBANTE
                        $modulo = 'registroUnico.plantelesPae.comprobante';
                        
                        /* BUSCA SI EL PLANTEL FUE ACTUALIZADO POR ALGUN USUARIO ACTUALMENTE.
                         * SI NO POSEE ALGUNA ACTUALIZACION, TOMA AL USUARIO QUE GENERO EL PLANTEL.
                         * EN CUALQUIERA DE LOS CASOS SE TOMA EL ID Y EL NOMBRE DEL USUARIO.
                        */
                        if ( !isset($model->usuario_act_id) )
                        {
                            $usuario = $model->usuario_ini_id;
                            $nombre = $model->usuarioIni->nombre;
                        }
                        else {
                            $usuario = $model->usuario_act_id;
                            $nombre = $model->usuarioAct->nombre;
                        }
                        
                        // LLAMA AL PROCEDIMIENTO QUE GENERA LA DATA DEL plantel_pae_comprobante Y VALIDA SI PUEDEN GENERAR UN COMPROBANTE PAE
                        $result = PlantelPae::model()->puedeObtenerComprobantePae($model->id, $modulo, $usuario, $nombre);
                        
                        //INICIO IF-1
                        //SI $result ES UN ARRAY Y EXISTE EL INDICE 'codigo' DENTRO DEL ARRAY Y ESE INDICE ES IGUAL A 'EXITO' EJECUTA EL BLOQUE DE CODIGO
                        if(is_array($result) && array_key_exists('codigo', $result) && $result['codigo']=='EXITO'){ 

                            //CAPTURA LOS VALORES DEL ARRAY
                            $codigo_seguridad = $result['codigo_seguridad'];
                            $archivo_pdf = $result['archivo_pdf'];
                            $fecha_vencimiento = $result['fecha_vencimiento'];
                            
                            //CAPTURA LAS RUTAS
                            $filePath = str_replace('//', '/', str_replace('//', '/', Yii::app()->params['downloadDirectoryPath'].'/comprobantesPae/').'/'.$archivo_pdf);
                            $qrCodePath = str_replace('//', '/', Yii::app()->params['downloadDirectoryPath'].'/comprobantesPae/qr/'.$codigo_seguridad.'.png');

                            //CAPTURA TODOS LOS DATOS DEL PLANTEL Y DE TODAS LAS TABLAS RELACIONADAS AL COMRPOBANTE
                            $plantel = PlantelPae::model()->getDataParaComprobanteConsole($model->id);
                            
                            //INICIO IF-2
                            //SI AUN NO SE A GENERADO EL PDF DEL COMPROBANTE
                            if(!file_exists($filePath) ){
                                
                                $proveedor = (in_array($plantel['siglas_proveedor_actual'], array('PDVAL', 'MERCAL')))?htmlentities($plantel['siglas_proveedor_actual']):htmlentities($plantel['razon_social_proveedor_actual']);
                                
                                $content = "";
                                
                                //INICIO IF-3
                                if($plantel){ 
                                    $codPlantel = (strpos($plantel['cod_plantel'], 'CNAE')===false)?htmlentities($plantel['cod_plantel']):'Sin Código DEA';
                                    $content = "---\n".'Código CNAE: '.$plantel['cod_cnae'].".\n".'Código DEA: '.$codPlantel.".\n".'Nombre de la Institución Educativa: '.$plantel['nombre_plantel'].".\n".'Cédula del Director: '.$plantel['origen_director'].'-'.$plantel['cedula_director'].".\n".'Nombre del Director: '.$plantel['nombre_director']." ".$plantel['apellido_director'].".\n".'Proveedor Actual: '.$proveedor.".\n".'Fecha de Vencimiento: '.$fecha_vencimiento.".\n".'Código de Seguridad: '.$codigo_seguridad."\n---";
                                } //FIN IF-3

                                //INICIO IF-3
                                if(!file_exists($qrCodePath)){
                                    //UTILIDAD QUE GENERA EL CODIGO QR DEL COMPROBANTE
                                    Utiles::generateQrCodeInFile($qrCodePath, $content);
                                }//FIN IF-3
                                
                                //SE INICIA LA GENERACION DEL PDF DEL COMPROBANTE
                                $mPDF = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 13, 13, 13, 13, 9, 9, 'M');
                                //CABECERA DEL PDF
                                $mPDF->WriteHTML($this->renderPartial('/plantelesPae/_pdfHeader', array(), true));
                                //FORMATO DEL CONTENIDO DEL PDF
                                $mPDF->WriteHTML($this->renderPartial('/plantelesPae/_viewPdfComprobante', array('plantel'=>$plantel, 'codigo_seguridad'=>$codigo_seguridad, 'fecha_vencimiento'=>$fecha_vencimiento), true, false));
                                //GENERA PDF
                                $mPDF->Output($filePath, EYiiPdf::OUTPUT_TO_FILE);
                                //LE DAN AL PDF TODOS LOS PERMISOS PARA LEER, ESCRIBIR Y BORRAR.
                                $command = 'chmod 777 -R '.$filePath;
                                exec($command); 

                           
                            }//FIN IF-2   
                        }//FIN IF-1
                        //ELSE DE IF-1
                        else{ 
                            //INICIO IF-2
                            if(isset($result['mensaje'])){
                                echo date('Y-m-d H:i:s').$result['mensaje'];
                            }//FIN IF-2
                            //ELSE DE IF-2 
                            else{
                                echo date('Y-m-d H:i:s').": Ha ocurrido un error en el sistema. Comuniquese con el administrador. \n";
                            }//FIN ELSE DE IF-2
                        }//FIN ELSE DE IF-1

                        //SI SE GENERA EL PDF
                        //INICIO IF-1
                        /* QUITAR 
                        if(is_file($directory.$result['archivo_pdf'])){
                            //CAPTURA LOS DATOS DEL PLANTEL Y DE LAS TABLAS RELACIONADAS A PLANTEL QUE SE USARAN PARA EL COMPROBANTE
                            $datosComprobante = Plantel::model()->getDatosComprobantePae($codigo_seguridad);
                            //ESTRUCTURA DEL CORREO
                            $contenido = $this->renderPartial('/plantelesPae/_correoComprobanteCnae', array(
                                'datosComprobante'=>$datosComprobante
                            ), true);
                            
                            //NOMBRE DEL DUEÑO DEL CORREO
                            $nombreDestinatario = (isset($result['origen_autoridad']) && isset($result['cedula_autoridad'])) ? $result['origen_autoridad'].'-'.$result['cedula_autoridad'] : null;
                            //CORREO 
                            $correDestinatario = (isset($comprobante['correo_autoridad'])) ? strtolower($comprobante['correo_autoridad']) : null;
                            //PLANTEL DEL COMPROBANTE
                            $plantel = (isset($comprobante['nombre'])) ? 'Director - '.$comprobante['nombre'] : null;
                            //PDF A ENVIAR
                            $archivo = $directory.$result['archivo_pdf'];
                            //ENVIA EL CORREO
                            $result = $this->enviarCorreo($correDestinatario, $plantel, 'CNAE - Sistema de Gestión Operativa del PAE | Comprobante de Beneficiario', $contenido, 'soporte_gescolar@me.gob.ve', 'CNAE - Sistema de Gestión Operativa del PAE', $archivo);
                            
                            //SI SE ENVIA EL CORREO
                            //INICIO IF-2
                            if($result){
                                //IDENTIFICA EN AL TABLA CUANDO EL COMPROBANTE FUE ENVIADO
                                $actualizacion = PlantelPaeComprobante::marcarComprobanteCnaeEnviado($comprobante['id']);
                                //INICIO IF-3
                                if($actualizacion){
                                    echo date('Y-m-d H:i:s').': ENVIO EXITOSO - ACTUALIZACION DE ESTATUS DE SOLICITUD DE COMPROBANTE EXITOSO'.json_encode($comprobante).".\n";
                                }//FIN IF-3
                                //INICIO ELSE DEL IF-3
                                else{
                                    echo date('Y-m-d H:i:s').': ENVIO EXITOSO - '.json_encode($comprobante).".\n";
                                }//FIN ELSE DEL IF-3
                            }//FIN IF-2
                            //INICIO ELSE DE IF-2
                            else{
                                echo date('Y-m-d H:i:s').': ENVIO FALLIDO - '.json_encode($comprobante).".\n";
                            }//FIN ELSE DE IF-2
                        }//FIN IF-1
                        //INICIO ELSE DE IF-1
                        else{
                            echo date('Y-m-d H:i:s').': ENVIO FALLIDO - NO EXISTE EL ARCHIVO ADJUNTO - '.json_encode($comprobante)."\n";
                        }//FIN ELSE DE IF-1
                        */
                        //CONTADOR PARA EL SLEEP()
                        $contador = $contador + 1;
                    }

                    echo date('Y-m-d H:i:s').": FIN DEL PROCESO DE ENVÍO DE CORREO CON COMPROBANTE CNAE.\n\n\n\n";
                    
        } catch (Exception $ex) {
            $respuesta['statusCode'] = 'error';
            $respuesta['error'] = $ex->getMessage();
            $respuesta['mensaje'] = "HA OCURRIDO UN ERROR DURANTE EL PROCESO DE ENVÍO DE CORREO DEL COMPROBANTE CNAE. {$respuesta['error']}.";
            echo date('Y-m-d H:i:s').": ERROR - ".$respuesta['mensaje'].'. Linea: Nro. '.$ex->getLine().".\n";
            echo date('Y-m-d H:i:s').": FIN DEL PROCESO DE ENVÍO DE CORREO DEL COMPROBANTE CNAE - CON ERROR.\n\n\n\n\n\n";
        }
    }

    public function getViewPath($module='') {
        $modulePath = '';
        if(strlen($module)>0){
            $modulePath = '/modules/'.$module;
        }
        return Yii::app()->getBasePath() . $modulePath . DIRECTORY_SEPARATOR . 'views';
    }
    
    /**
     * 
     * @param type $to
     * @param type $subject
     * @param type $msj
     * @param type $from
     * @param type $from_name
     * @param type $archivo
     * @param type $nombre_archivo
     * @return type
     */
    static public function enviarCorreo($to, $to_name, $subject = 'SIR-SWL', $msj = '', $from = '', $from_name = '',$archivo=null,$nombre_archivo=null) {
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $mailer->Host = 'mail.me.gob.ve:25';
        $mailer->SMTPDebug  = 2;
        $mailer->IsSMTP();
        
        // $mailer->SMTPAuth   = true;                  // enable SMTP authentication
        // $mailer->SMTPSecure = "tls";                 // sets the prefix to the servier
        // $mailer->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        // $mailer->Port       = 587;                   // set the SMTP port for the GMAIL server
        // //$mailer->Username   = "soporte_gopae_cnae@gmail.com";  // GMAIL username
        // $mailer->Username   = "gescolar.mppe@gmail.com";  // GMAIL username
        // $mailer->Password   = "gescolarpro";            // GMAIL password
        
        if (is_array($to)) {
            foreach ($to as $sendTo) {
                $mailer->AddAddress($sendTo);
            }
        } else {
            echo 'Email to: '.$to.'. Name To: '.$to_name."\n";
            $mailer->AddAddress($to, $to_name);
        }

        if (isset($from) and $from != '' and $from != null)
            $mailer->From = $from;
        else
            $mailer->From = Yii::app()->params->adminEmail;
        if (isset($from_name) and $from_name != '' and $from_name != null)
            $mailer->FromName = $from_name;
        else
            $mailer->FromName = Yii::app()->params->adminName;
        if($archivo){
            //$mailer->AddBCC('soporte_gescolar@me.gob.ve');
            //$mailer->AddBCC('jarojasm@me.gob.ve');
            $mailer->AddBCC(Yii::app()->params['adminGmail']);
            $mailer->AddAttachment($archivo);
        }
        
        $mailer->Username = 'soporte_gescolar';

        $mailer->CharSet = 'UTF-8';
        $mailer->Subject = $subject;
        $mailer->MsgHTML($msj);
        return $mailer->Send();
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath($this->module) . $viewName . '.php';
    }

    /**
     * Modeified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }
 
}
