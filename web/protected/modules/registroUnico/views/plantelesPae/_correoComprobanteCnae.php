<img width="600" src="http://me.gob.ve/images/bar_gob.png">
    <br/>
    <div align="left">
        Un saludo cordial del Ministerio del Poder Popular para la Educación (MPPE) y la Corporación Nacional de Alimentación Escolar (CNAE S.A.).
        <br/><br/>
        Adjunto se encuentra el Comprobante CNAE <b><?php echo $datosComprobante['codigo_seguridad']; ?></b> de la Institución Educativa <b>
        <?php echo $datosComprobante['nombre']; ?></b> con el Código CNAE <?php echo $datosComprobante['cod_cnae']; ?>.
        <br/>
        El comprobante emitido es válido en el Periodo Escolar <?php echo $datosComprobante['periodo']; ?> 
        y hasta la fecha <?php echo $datosComprobante['fecha_caducidad']; ?>.
    </div>
    <br/><br/>
    <div align="center" style="color: #222A2D; font-size: 10px;">
        "Todos los niños tienen derecho desde el momento de nacer, por nacer y por ser seres humanos, 
        el derecho a la educación; es un derecho natural. No podemos privatizar ni eliminar ese derecho" 
        <br/>
        Hugo Chávez Frías
    </div>
    <br/><br/>
    <div align="left" style="color: #990000;">
        ---
        Corporación Nacional de Alimentación Escolar S.A.
        <br/>
        Sistema de Gestión Operativa del Programa de Alimentación Escolar.
    </div>
    <br/></br><br/></br>
    <div align="center" style="color: green; font-size: 10px;">
        Si no es necesaria la impresión de este correo, evite imprimirlo. Todos somos responsables del cuidado del medio ambiente.
        <br/>
        Un mensaje de Fundabit.
    </div>
    <br/>
    <div align="center" style="color: #222A2D; font-size: 10px;">
        Este correo es enviado a los directores de las instituciones educativas. Si usted ha recibido este correo de forma equivocada notifíquelo al correo soporte_gescolar@me.gob.ve y luego elimine su contenido.
        <br/>
        El contenido absoluto de este correo y sus adjuntos pertenecen al Ministerio del Poder Popular para La Educación y la Corporación 
        Nacional de Alimentación Escolar cualquier difusión de esta información a entes externos sin la debida autorización del Ministerio 
        recurrirá a la apertura de procesos administrativos y legales.
        <br/>
        Evite ser sancionado.
    </div>
       