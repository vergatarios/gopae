# Proceso de Egreso de Talento Humano

El Proceso de Egreso de Talento Humano viene dado por hecho de que debido a un motivo un empleado rompe relaciones laborales, en este caso, con la Corporación. Es requerido que este proceso debe ser registrado en el sistema mediante un proceso masivo. Los cambios que actualmente se podrán apreciar son netamente a nivel de desvinculación del personal con la empresa sin llegar a los cálculos de liquidaciones ni otros pagos que se deben efectuar al empleado que egresa.

Para este proceso de Egresos es necesario poder contar con los siguientes datos:
  - Origen (V, E, P)
  - Número de Cédula de Identidad
  - Estado (Amazonas, Bolívar, Distrito Capital...)
  - Motivo (Renuncia, Culminación de Contrato...)
  - Observación o Descripción del Motivo
  - Fecha de Egreso (Esta no debe ser antes del 01/09/2015 ni después del día actual que se efectúe el Registro del Egreso)
  - Solicitado por: Debido a que regularmente los egresos de un Cocinero o Cocinera Escolar no se hacen de forma personal, alguien con autoridad debe avalar la solicitud de egreso de un personal. Acá se debe ingresar el nombre e identificación de la persona que aprueba el egreso.

Una vez proveido estos datos se procederá al proceso de verificación de dichos datos:

   - En primera instancia se deben validar que el estado y el motivo indicado aparezcan en nuestras tablas catálogos destinadas a estas dos entidades respectivamente.
   - Se Validará la fecha de egreso.
   - Se validarán los tipos de datos de los demás campos.

Luego de realizar las validaciones básicas del lado de la aplicación (PHP) se podrán enviar los datos al procedimiento almacenado que efectúa el proceso de egreso, en donde se efectuarán otras validaciones como:

   - Que el el Talento Humano con la Cédula de Identidad Indicada se encuentra registrado en el Sistema. (Ver Error E0001 en el SP)
   - La Persona posee un estatus de empleado que indica que pertenece a la corporación. (Ver Error E0002 en el SP)
   - La Persona posee registro de ingreso a la corporación. (Ver Error E0003 en el SP)
   - La Persona pertenece al Estado Indicado. (Ver Error E0004 en el SP)

Luego de haber validado esto se efectúan un conjunto de operaciones sobre varias tablas en la base de datos del Sistema de Gestión Operativa del CNAE:

### gestion_humana.egreso_empleado
Se "Inserta" en esta tabla el registro del egreso del empleado, para esto deben tener los siguientes datos:
   - ID del Talento Humano
   - ID del Motivo de Egreso
   - ID del Último Ingreso Registrado
   - Fecha del Egreso
   - Descripción del Motivo del Egreso
   - Nombre e Identificación de la Persona que aprueba o solicita el Egreso.

### gestion_humana.talento_humano
Se debe hacer constar el estatus actual de egresado de la persona, para esto se "actualizarán" algunos datos:
   - Código de Integridad de los Datos
   - Fecha de Egreso
   - ID Plantel Actual a "NULL" si la persona es una cocinera o cocinero escolar.
   - Estatus actual a "F" (Ver tabla gestion_humana.estatus_empleado)
   
### gestion_humana.cocinera_plantel
Si la persona es una cocinera o cocinero escolar en esta tabla debe actualizar el estatus del registro que vincula a esta persona a un plantel, colocando el estatus de ese registro en un valor "E".

### auditoria.traza
Como todo proceso en el sistema debe quedar la traza del proceso efectuado en la tabla de auditoria.traza.

El Procedimiento almacenado que efectúa este proceso tiene la siguiente identificación:
```sql
    CREATE OR REPLACE FUNCTION gplantel.validate_asignacion_madre_cocinera(
        origen_vi character varying,
        cedula_vi integer,
        estado_id_vi integer,
        motivo_egreso_id_vi integer,
        motivo_descripcion_vi text,
        fecha_egreso_vi date,
        solicitado_por_vi character varying,
        modulo_vi character varying,
        usuario_vi integer,
        username_vi character varying,
        ipaddress_vi character varying)
    RETURNS record ...
```

José Gabriel González Pérez
31-03-2015

Licencia
----
MIT

**Software Libre!**

[José Gabriel González]:jgonzalezp@me.gob.ve
