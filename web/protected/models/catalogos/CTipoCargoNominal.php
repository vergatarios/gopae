<?php
/**
 * Catalogo de $nombreClass
 *
 * @author Generador de Código
 */
class CTipoCargoNominal extends CCatalogo { 

    protected static $columns = 
        array (
  0 => 'id',
  1 => 'codigo',
  2 => 'nombre',
  3 => 'siglas',
  4 => 'descripcion',
  5 => 'funciones',
  6 => 'condicion_id',
);

    /**
     * Setea la data en una propiedad static llamada data
     */
    protected static function setData(){

        self::$data = 
        array (
  0 => 
  array (
    'id' => 5,
    'codigo' => 'TC0005',
    'nombre' => 'Adjunto',
    'siglas' => 'ASGCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 3,
  ),
  1 => 
  array (
    'id' => 9,
    'codigo' => 'TC0009',
    'nombre' => 'Bachiller',
    'siglas' => 'BCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 4,
  ),
  2 => 
  array (
    'id' => 6,
    'codigo' => 'TC0006',
    'nombre' => 'Coordinador',
    'siglas' => 'CCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 3,
  ),
  3 => 
  array (
    'id' => 3,
    'codigo' => 'TC0003',
    'nombre' => 'Gerente',
    'siglas' => 'GCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 2,
  ),
  4 => 
  array (
    'id' => 2,
    'codigo' => 'TC0002',
    'nombre' => 'Gerente General',
    'siglas' => 'GGCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 2,
  ),
  5 => 
  array (
    'id' => 10,
    'codigo' => 'TC0010',
    'nombre' => 'Inspectores',
    'siglas' => 'ICNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 4,
  ),
  6 => 
  array (
    'id' => 11,
    'codigo' => 'TC0011',
    'nombre' => 'Obrero',
    'siglas' => 'OCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 4,
  ),
  7 => 
  array (
    'id' => 1,
    'codigo' => 'TC0001',
    'nombre' => 'Presidente de la Corporación',
    'siglas' => 'PCNAE',
    'descripcion' => '',
    'funciones' => '',
    'condicion_id' => 2,
  ),
  8 => 
  array (
    'id' => 7,
    'codigo' => 'TC0007',
    'nombre' => 'Profesional',
    'siglas' => 'PRCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 4,
  ),
  9 => 
  array (
    'id' => 4,
    'codigo' => 'TC0004',
    'nombre' => 'Sub-Gerente',
    'siglas' => 'SGCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 3,
  ),
  10 => 
  array (
    'id' => 8,
    'codigo' => 'TC0008',
    'nombre' => 'Técnico',
    'siglas' => 'TCNAE',
    'descripcion' => NULL,
    'funciones' => NULL,
    'condicion_id' => 4,
  ),
)		; 

    	}
}
