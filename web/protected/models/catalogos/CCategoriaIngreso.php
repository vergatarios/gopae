<?php
/**
 * Catalogo de $nombreClass
 *
 * @author Generador de Código
 */
class CCategoriaIngreso extends CCatalogo { 

    protected static $columns = 
        array (
  0 => 'id',
  1 => 'nombre',
  2 => 'siglas',
  3 => 'descripcion',
);

    /**
     * Setea la data en una propiedad static llamada data
     */
    protected static function setData(){

        self::$data = 
        array (
  0 => 
  array (
    'id' => 4,
    'nombre' => 'Comisión de Servicio',
    'siglas' => 'CS',
    'descripcion' => NULL,
  ),
  1 => 
  array (
    'id' => 3,
    'nombre' => 'Empleado de la Corporación',
    'siglas' => 'EM',
    'descripcion' => NULL,
  ),
  2 => 
  array (
    'id' => 5,
    'nombre' => 'Encargaduría',
    'siglas' => 'EN',
    'descripcion' => NULL,
  ),
  3 => 
  array (
    'id' => 2,
    'nombre' => 'Honorarios Profesionales',
    'siglas' => 'HP',
    'descripcion' => NULL,
  ),
  4 => 
  array (
    'id' => 1,
    'nombre' => 'Tercerizado',
    'siglas' => 'CT',
    'descripcion' => NULL,
  ),
)		; 

    	}
}
