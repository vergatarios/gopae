<?php

class IngresoMasivoTalentoHumano extends TalentoHumano {
    
    
    public function getIdTalentoHumano($origen, $cedula){
        $sql = "SELECT id FROM gestion_humana.talento_humano WHERE origen = :origen AND cedula = :cedula AND estatus = 'A' LIMIT 1";
        
        $guard = Yii::app()->db->createCommand($sql);
        $guard->bindParam(':origen',$origen,PDO::PARAM_STR);
        $guard->bindParam(':cedula',$cedula,PDO::PARAM_INT);
        $resultado = $guard->queryScalar();
        
        return $resultado;
    }
    
    
    public function loadRegistroIngresoMasivo($sheetData, $usuarioId, $usuario, $ipUsuario, $modulo) {
        
        
        $response = new stdClass();

        $response->data = new stdClass();
        $response->data = array();
        $i = 2; //$i sirve para recorrer el excel
        $j = $i - 2; // $j sirve para crear arreglos de respuesta

        while (array_key_exists($i, $sheetData) && trim($sheetData[$i]['A']) != '') {

            $response->data[$j] = new stdClass();

            //imprimimos el contenido de la celda utilizando la letra de cada columna
            $objCell = $sheetData[$i];
            $nroContrato = strtoupper(trim($objCell['A']));
            $fechaIngresoUnix = strtoupper(utf8_decode(trim($objCell['B'])));
            $origen = strtoupper(utf8_decode(trim($objCell['C'])));
            $cedula = utf8_decode(trim($objCell['D']));
            $nombre = strtoupper(utf8_decode(trim($objCell['E'])));
            $apellido = strtoupper(utf8_decode(trim($objCell['F'])));
            $correo = strtoupper(utf8_decode(trim($objCell['G'])));
            $telefonoFijo = strtoupper(trim($objCell['H']));
            $telefonoCeluar = strtoupper(trim($objCell['I']));
            $fechaNacimientoUnix = strtoupper(utf8_decode(trim($objCell['J'])));
            $estado = strtoupper(utf8_decode(trim($objCell['K'])));
            $municipio = strtoupper(utf8_decode(trim($objCell['L'])));
            $categoriaIngreso = utf8_decode(trim($objCell['M']));
            $tipoCargoNominal = utf8_decode(trim($objCell['N']));
            $cargoNominal = utf8_decode(trim($objCell['O']));
            $estructuraOrganizativa = utf8_decode(trim($objCell['P']));
            $condicionNominal = utf8_decode(trim($objCell['Q']));
            $tipoNomina = utf8_decode(trim($objCell['R']));
            $banco = utf8_decode(trim($objCell['S']));
            $tipoCuentaBanco = utf8_decode(trim($objCell['T']));
            $tipoSerialCuentaBanco = utf8_decode(trim($objCell['U']));
            $numeroCuentaBanco = strtoupper(trim($objCell['V']));
            $codigoPlantel = utf8_decode(trim($objCell['W']));
            $observacion = utf8_decode(trim($objCell['X']));
            
            //Transformar los datos
            if(is_numeric($fechaIngresoUnix)) {
                $fechaIngreso = gmdate("d-m-Y", $fechaIngresoUnix);
            } else {
                $fechaIngreso = date('d-m-Y', $fechaIngresoUnix*1);
            }
            
            if(is_numeric($fechaNacimientoUnix)) {
                $fechaNacimiento = gmdate("d-m-Y",$fechaNacimientoUnix);
            } else {
                $fechaNacimiento = date('d-m-Y', $fechaNacimientoUnix*1);
            }
            
            $estado = str_replace('_', ' ', $estado);
            $estadoData = CEstado::getData('nombre', $estado);
            $estadoId = (isset($estadoData[0]))?$estadoData[0]['id']:null;

            $municipio = str_replace('_', ' ', $municipio);
            $municipioData = CEstado::getData('nombre', $estado);
            $municipioId = (isset($municipioData[0]))?$municipioData[0]['id']:null;
            
            
            $categoriaIngresoData = CCategoriaIngreso::getData('nombre', utf8_encode($categoriaIngreso));
            $categoriaIngresoId = (isset($categoriaIngresoData[0]))?$categoriaIngresoData[0]['id']:null;

            $tipoCargoNominalData = CTipoCargoNominal::getData('nombre', utf8_encode($tipoCargoNominal));
            $tipoCargoNominalId = (isset($tipoCargoNominalData[0]))?$tipoCargoNominalData[0]['id']:null;
            
            $cargoNominalData = CCargoNominal::getData('nombre',utf8_encode($cargoNominal));
            $cargoNominalId = (isset($cargoNominalData[0]))?$cargoNominalData[0]['id']:null;
            //var_dump($objCell['O'], $cargoNominal, $cargoNominalData, $cargoNominalId);die();

            $estructuraOrganizativaData = CEstructuraOrganizativa::getData('nombre', utf8_encode($estructuraOrganizativa));
            $estructuraOrganizativaId = (isset($estructuraOrganizativaData[0]))?$estructuraOrganizativaData[0]['id']:null;
            
            $condicionNominalData = CCondicionNominal::getData('nombre', utf8_encode($condicionNominal));
            $condicionNominalId = (isset($condicionNominalData[0]))?$condicionNominalData[0]['id']:null;
            
            $tipoNominaData = CTipoNomina::getData('nombre', utf8_encode($tipoNomina));
            $tipoNominaId = (isset($tipoNominaData[0]))?$tipoNominaData[0]['id']:null;
            
            $bancoData = CBanco::getData('nombre', utf8_encode($banco));
            $bancoId = (isset($bancoData[0]))?$bancoData[0]['id']:null;
        
            $tipoCuentaBancoData = CTipoCuenta::getData('nombre', utf8_encode($tipoCuentaBanco));
            $tipoCuentaBancoId = (isset($tipoCuentaBancoData[0]))?$tipoCuentaBancoData[0]['id']:null;

            $tipoSerialCuentaBancoData = CTipoSerialCuenta::getData('nombre', utf8_encode($tipoSerialCuentaBanco));
            $tipoSerialCuentaBancoId = (isset($tipoSerialCuentaBancoData[0]))?$tipoSerialCuentaBancoData[0]['id']:null;
            
            $plantelId = $this->getPlantelId($codigoPlantel);

            
            // Colocando los Datos leidos de la hoja de cálculo en la respuesta
            $response->data[$j]->cedula = "$origen-$cedula";
            $response->data[$j]->estado = $estado;
            $response->data[$j]->fechaIngreso = $fechaIngreso;

            //Empezamos con los filtros y validaciones para luego procesar los registros de Egresos de Talento Humano
            if (in_array($origen, array('V', 'E', 'P'))){ // Se valida que los valores contenidos en el campo origen sean válidos
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 10;
                $response->data[$j]->mensaje              = "El Campo 'Origen' debe contener uno de estos dos caracteres V ó E";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!helper::isValidDate($fechaIngreso, 'd-m-Y')){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 11;
                $response->data[$j]->mensaje              = "No se ha recibido una Fecha de Ingreso en el formato válido (DD/MM/AAAA). Fecha leida del archivo: '.$fechaIngreso.'.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!helper::isValidDate($fechaNacimiento, 'd-m-Y')){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 11;
                $response->data[$j]->mensaje              = "No se ha recibido una Fecha de Nacimiento en el formato válido (DD/MM/AAAA). Fecha leida del archivo: '.$fechaNacimiento.'.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($cedula)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 12;
                $response->data[$j]->mensaje              = "El Número de Cédula debe contener solo caracteres numéricos";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($estadoId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 13;
                $response->data[$j]->mensaje              = "El Estado Ingresado no es válido. Asegurese de Seleccionar un Estado válido del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($municipioId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 14;
                $response->data[$j]->mensaje              = "El Municipio Ingresado no es válido. Asegurese de Seleccionar un Municipio válido del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($categoriaIngresoId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 15;
                $response->data[$j]->mensaje              = "La Categoria de Ingreso que ha Ingresado no es válida. Asegurese de Seleccionar una Categoria válida del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($tipoCargoNominalId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 16;
                $response->data[$j]->mensaje              = "El Tipo de Cargo Nominal que ha Ingresado no es válida. Asegurese de Seleccionar un Tipo de Cargo Nominal válida del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($cargoNominalId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 17;
                $response->data[$j]->mensaje              = "El Cargo Nominal que ha Ingresado no es válida. Asegurese de Seleccionar un Cargo Nominal válida del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($estructuraOrganizativaId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 18;
                $response->data[$j]->mensaje              = "La Estructura Organizativa que ha Ingresado no es válida. Asegurese de Seleccionar una Estructura Organizativa válida del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($condicionNominalId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 19;
                $response->data[$j]->mensaje              = "La Condicion Nominal que ha Ingresado no es válida. Asegurese de Seleccionar una Condicion Nominal válida del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($tipoNominaId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 20;
                $response->data[$j]->mensaje              = "El Tipo de Nomina que ha Ingresado no es válida. Asegurese de Seleccionar un Tipo de Nomina válido del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($bancoId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 21;
                $response->data[$j]->mensaje              = "El Banco que ha Ingresado no es válida. Asegurese de Seleccionar un Banco válido del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($tipoCuentaBancoId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 22;
                $response->data[$j]->mensaje              = "El Tipo de Cuenta del Banco que ha Ingresado no es válida. Asegurese de Seleccionar un Tipo de Cuenta del Banco válido del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($tipoSerialCuentaBancoId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 23;
                $response->data[$j]->mensaje              = "El Tipo Serial de la Cuenta del Banco que ha Ingresado no es válida. Asegurese de Seleccionar un Tipo Serial de la Cuenta del Banco válido del Formato Excel proporcionado.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($plantelId)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 24;
                $response->data[$j]->mensaje              = "El Codigo del plantel que ha Ingresado no es válido. Asegurese de Ingresar un Codigo válido del plantel.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($nroContrato)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 25;
                $response->data[$j]->mensaje              = "El Numero de Contrato Ingresado no es válido. Asegurese de Ingresar un Numero de Contrato válido.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(!is_numeric($numeroCuentaBanco)){ 
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 26;
                $response->data[$j]->mensaje              = "El Numero de Contrato que ha Ingresado no es válido. Asegurese de Ingresar un Numero de Contrato válido.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            //------------------------------------------------
            /*
            } elseif(strlen($descripcionEgreso)){ // Se valida la descripción del motivo sea un campo lleno
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 15;
                $response->data[$j]->mensaje              = "La Descripción del Motivo de Egreso del Talento Humano es un campo obligatorio.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            } elseif(strlen($solicitadoPor)){ // Se valida que el campo solicitado por esté lleno
                $result = false;
                $response->data[$j]->proceso_exitoso      = 0;
                $response->data[$j]->codigo_resultado     = 16;
                $response->data[$j]->mensaje              = "Es obligatorio indicar quién ha solicitado el egreso de este talento humano.";
                $response->data[$j]->resultado            = "<span style='color:#FF0000;'><b>EGRESO NO REGISTRADO</b></span>";
            
            */
            } else {
                
                $poseeNumeroContrato = ( $nroContrato == null OR !IS_NUMERIC($nroContrato) )?'NO':'SI';
                $origen = substr($origen, 0, 1);
                $talentoHumanoId = $this->getIdTalentoHumano($origen, $cedula);
                
               /*$this->testSpIngresos(
                    $talentoHumanoId,
                    $poseeNumeroContrato,
                    $nroContrato,
                    $fechaIngreso,
                    $categoriaIngresoId,
                    $tipoCargoNominalId,
                    $cargoNominalId,
                    $estructuraOrganizativaId,
                    $condicionNominalId,
                    $tipoNominaId,
                    $plantelId,
                    $observacion,
                    $usuarioId,
                    $modulo,
                    $ipUsuario
                );*/
                
                $spResponse =   IngresoEmpleado::model()->ingresoMadreTrabajadora(
                                    $talentoHumanoId,
                                    $poseeNumeroContrato,
                                    $nroContrato,
                                    $fechaIngreso,
                                    $categoriaIngresoId,
                                    $tipoCargoNominalId,
                                    $cargoNominalId,
                                    $estructuraOrganizativaId,
                                    $condicionNominalId,
                                    $tipoNominaId,
                                    $plantelId,
                                    $observacion,
                                    $usuarioId,
                                    $modulo,
                                    $ipUsuario
                                );
                
                if($spResponse['resultado']=='success'){
                    $response->data[$j]->proceso_exitoso = 1;
                }
                elseif($spResponse['resultado']=='error'){
                    $response->data[$j]->proceso_exitoso = 0;
                }
                else{
                    $response->data[$j]->proceso_exitoso = 2;
                }
                    
                $response->data[$j]->codigo_resultado = $spResponse['status'];
                $response->data[$j]->mensaje = $spResponse['mensaje'];
                
                $result = ($response->data[$j]->proceso_exitoso)?true:false;

                $response->data[$j]->response = $spResponse;

                if ($response->data[$j]->proceso_exitoso == 1) {
                    $response->data[$j]->resultado = "<span style='color:#33DD00;'>CARGADO</span>";
                } elseif ($response->data[$j]->proceso_exitoso == 0) {
                    $response->data[$j]->resultado = "<span style='color:#FF0000;'>NO CARGADO</span>";
                } else {
                    $response->data[$j]->resultado = "<span style='color:#66AA00;'>CON ADVERTENCIAS</span>";
                }
            }

            $i++;
            $j = $i - 2;
        }
        return array($result, $response->data);
    }
    
    
    private function testSpIngresos(
        $talentoHumanoId,
        $poseeNumeroContrato,
        $nroContrato,
        $fechaIngreso,
        $categoriaIngresoId,
        $tipoCargoNominalId,
        $cargoNominalId,
        $estructuraOrganizativaId,
        $condicionNominalId,
        $tipoNominaId,
        $plantelId,
        $observacion,
        $usuarioIniId,
        $model,
        $ipReal
    ){
        $spTesting = $this->getPrintSpIngreso(
            $talentoHumanoId,
            $poseeNumeroContrato,
            $nroContrato,
            $fechaIngreso,
            $categoriaIngresoId,
            $tipoCargoNominalId,
            $cargoNominalId,
            $estructuraOrganizativaId,
            $condicionNominalId,
            $tipoNominaId,
            $plantelId,
            $observacion,
            $usuarioIniId,
            $model,
            $ipReal
        );
        echo "<pre>$spTesting</pre>";
        die();
    }
    
    public function getPrintSpIngreso(
        $talentoHumanoId,
        $poseeNumeroContrato,
        $nroContrato,
        $fechaIngreso,
        $categoriaIngresoId,
        $tipoCargoNominalId,
        $cargoNominalId,
        $estructuraOrganizativaId,
        $condicionNominalId,
        $tipoNominaId,
        $plantelId,
        $observacion,
        $usuarioIniId,
        $model,
        $ipReal
    ) {
        return "SELECT
                        *
                    FROM
                        gestion_humana.ingresos_empleado( 
                            $talentoHumanoId::int,
                            '$poseeNumeroContrato'::varchar,
                            '$nroContrato'::varchar,
                            '$fechaIngreso'::date,
                            $categoriaIngresoId::int,
                            $tipoCargoNominalId::int,
                            $cargoNominalId::int,
                            $estructuraOrganizativaId::int,
                            $condicionNominalId::int,
                            $tipoNominaId::int,
                            $plantelId::int,
                            '$observacion'::varchar,
                            $usuarioIniId::int,
                            '$model'::varchar,
                            '$ipReal'::varchar
                    ) AS f(
                        mensaje text,
                        resultado text,
                        status int,
                        id int
                    );
                    ";
    }

    
    public function getPlantelId($codigo) {
        $sql = "SELECT id FROM gplantel.plantel WHERE cod_plantel = :codigo AND estatus = 'A' LIMIT 1";
        
        $guard = Yii::app()->db->createCommand($sql);
        $guard->bindParam(':codigo',$codigo,PDO::PARAM_STR);
        $resultado = $guard->queryScalar();
        
        return $resultado;
    }
    
}