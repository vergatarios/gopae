<?php
/**
 * Catalogo de $nombreClass
 *
 * @author Generador de Código
 */
class CEstatusEmpleado extends CCatalogo { 

    protected static $columns = 
        array (
  0 => 'id',
  1 => 'nombre',
  2 => 'sigla',
  3 => 'estatus',
);

    /**
     * Setea la data en una propiedad static llamada data
     */
    protected static function setData(){

        self::$data = 
        array (
  0 => 
  array (
    'id' => 7,
    'nombre' => 'ABANDONO DEL CARGO',
    'sigla' => 'D',
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 6,
    'nombre' => 'AÑO SABATICO',
    'sigla' => 'B',
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 1,
    'nombre' => 'ASPIRANTE',
    'sigla' => 'A',
    'estatus' => 'A',
  ),
  3 => 
  array (
    'id' => 8,
    'nombre' => 'CLAUSULA 95',
    'sigla' => 'C',
    'estatus' => 'A',
  ),
  4 => 
  array (
    'id' => 4,
    'nombre' => 'COMISION DE SERVICIO',
    'sigla' => 'S',
    'estatus' => 'A',
  ),
  5 => 
  array (
    'id' => 14,
    'nombre' => 'EGRESADO',
    'sigla' => 'F',
    'estatus' => 'A',
  ),
  6 => 
  array (
    'id' => 2,
    'nombre' => 'EMPLEADO ACTIVO',
    'sigla' => 'E',
    'estatus' => 'A',
  ),
  7 => 
  array (
    'id' => 9,
    'nombre' => 'EN PROCESO DE INCAPACIDAD',
    'sigla' => 'I',
    'estatus' => 'A',
  ),
  8 => 
  array (
    'id' => 10,
    'nombre' => 'JUBILADO NO EXCLUIDO DE NOMINA',
    'sigla' => 'J',
    'estatus' => 'A',
  ),
  9 => 
  array (
    'id' => 5,
    'nombre' => 'LICENCIA SINDICAL',
    'sigla' => 'L',
    'estatus' => 'A',
  ),
  10 => 
  array (
    'id' => 11,
    'nombre' => 'PERMISOS ESPECIALES',
    'sigla' => 'P',
    'estatus' => 'A',
  ),
  11 => 
  array (
    'id' => 12,
    'nombre' => 'RENUNCIA NO EXCLUIDO DE NOMINA',
    'sigla' => 'R',
    'estatus' => 'A',
  ),
  12 => 
  array (
    'id' => 3,
    'nombre' => 'REPOSO MEDICO',
    'sigla' => 'M',
    'estatus' => 'A',
  ),
  13 => 
  array (
    'id' => 13,
    'nombre' => 'SIN UBICACION',
    'sigla' => 'O',
    'estatus' => 'A',
  ),
)		; 

    	}
}
