<?php

/**
 * Catalogo de $nombreClass
 *
 * @author Generador de Código
 */
class CMotivoEgreso extends CCatalogo {

    protected static $columns = array(
        0 => 'id',
        1 => 'nombre',
        2 => 'descripcion',
        3 => 'estatus',
    );

    /**
     * Setea la data en una propiedad static llamada data
     */
    protected static function setData() {

        self::$data = array(
                    0 =>
                    array(
                        'id' => 9,
                        'nombre' => 'Destitución',
                        'descripcion' => 'El Empleado ha sido destituido de su cargo',
                        'estatus' => 'A',
                    ),
                    1 =>
                    array(
                        'id' => 8,
                        'nombre' => 'Egreso por Pensión de Invalidez',
                        'descripcion' => 'El Empleado ha egresado debido a una invalidez',
                        'estatus' => 'A',
                    ),
                    2 =>
                    array(
                        'id' => 6,
                        'nombre' => 'Fallecimiento',
                        'descripcion' => 'El Empleado ha fallecido',
                        'estatus' => 'A',
                    ),
                    3 =>
                    array(
                        'id' => 3,
                        'nombre' => 'Faltas Injustificadas',
                        'descripcion' => 'El Empleado ha tenido Faltas reiteradas e injustificadas',
                        'estatus' => 'A',
                    ),
                    4 =>
                    array(
                        'id' => 10,
                        'nombre' => 'Fin de Contrato',
                        'descripcion' => 'El Contrato del Empleado ha llegado a su fin',
                        'estatus' => 'A',
                    ),
                    5 =>
                    array(
                        'id' => 2,
                        'nombre' => 'Incumplimiento de Contrato',
                        'descripcion' => 'El Empleado ha incumplido los acuerdos de su contrato',
                        'estatus' => 'A',
                    ),
                    6 =>
                    array(
                        'id' => 4,
                        'nombre' => 'Jubilación',
                        'descripcion' => 'El Empleado ya se encuentra en edad de jubilación',
                        'estatus' => 'A',
                    ),
                    7 =>
                    array(
                        'id' => 1,
                        'nombre' => 'Renuncia',
                        'descripcion' => 'El Empleado ha Renunciado',
                        'estatus' => 'A',
                    ),
                    8 =>
                    array(
                        'id' => 7,
                        'nombre' => 'Renuncia por el Artículo 32',
                        'descripcion' => 'El Empleado ha Renunciado basando dicha renuncia en el artículo 32 de LOT',
                        'estatus' => 'A',
                    ),
                    9 =>
                    array(
                        'id' => 11,
                        'nombre' => 'Retiro por Libre Nombramiento y Remoción',
                        'descripcion' => 'El Empleado ha egresado por poseer un cargo de Libre Nombramiento y Remoción',
                        'estatus' => 'A',
                    ),
                    10 =>
                    array(
                        'id' => 12,
                        'nombre' => 'Retiro por No Superar el Periodo de Prueba',
                        'descripcion' => 'El Empleado no ha superado el periódo de Prueba',
                        'estatus' => 'A',
                    ),
                    11 =>
                    array(
                        'id' => 5,
                        'nombre' => 'Retiro por Reducción de Personal',
                        'descripcion' => 'El Empleado ha sido despedido por motivos de reduccción depersonal',
                        'estatus' => 'A',
                    ),
                );
    }

}
