<?php
/**
 * Catalogo de $nombreClass
 *
 * @author Generador de Código
 */
class CTipoNomina extends CCatalogo { 

    protected static $columns = 
        array (
  0 => 'id',
  1 => 'codigo',
  2 => 'nombre',
  3 => 'tipo_periodo_id',
  4 => 'descripcion',
  5 => 'funciones',
);

    /**
     * Setea la data en una propiedad static llamada data
     */
    protected static function setData(){

        self::$data = 
        array (
  0 => 
  array (
    'id' => 1,
    'codigo' => 'TN0001',
    'nombre' => 'Nómina Central',
    'tipo_periodo_id' => 1,
    'descripcion' => NULL,
    'funciones' => NULL,
  ),
  1 => 
  array (
    'id' => 2,
    'codigo' => 'TN0002',
    'nombre' => 'Nómina Cuenta Social',
    'tipo_periodo_id' => 1,
    'descripcion' => NULL,
    'funciones' => NULL,
  ),
  2 => 
  array (
    'id' => 3,
    'codigo' => 'TN0003',
    'nombre' => 'Nómina Cuentas Corrientes',
    'tipo_periodo_id' => 1,
    'descripcion' => NULL,
    'funciones' => NULL,
  ),
  3 => 
  array (
    'id' => 4,
    'codigo' => 'TN0004',
    'nombre' => 'Nómina Pasiva',
    'tipo_periodo_id' => 1,
    'descripcion' => NULL,
    'funciones' => NULL,
  ),
)		; 

    	}
}
