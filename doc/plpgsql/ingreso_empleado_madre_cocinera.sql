-- Function: gestion_humana.ingresos_empleado(integer, character varying, character varying, date, integer, integer, integer, integer, integer, integer, integer, character varying, integer, character varying, character varying)

-- DROP FUNCTION gestion_humana.ingresos_empleado(integer, character varying, character varying, date, integer, integer, integer, integer, integer, integer, integer, character varying, integer, character varying, character varying);

CREATE OR REPLACE FUNCTION gestion_humana.ingresos_empleado(talento_humano_vi integer, posee_numero_contrato_vi character varying, nro_contrato_vi character varying, fecha_ingreso_vi date, categoria_ingreso_id_vi integer, tipo_cargo_nomina_id_vi integer, cargo_nomina_id_vi integer, estructura_organizativa_id_vi integer, condicion_nomina_id_vi integer, tipo_nomina_id_vi integer, plantel_id_vi integer, observaciones_vi character varying, usuario_ini_id_vi integer, modulo_vi character varying, ipaddress_vi character varying)
  RETURNS record AS
$BODY$
    DECLARE

        --VARIABLES GENERALES
        fechaIni_vi date := date('today');
        estatus_vi varchar := 'A';
        username_vi VARCHAR := '';

        --VARIABLES DE MENSAJE
        status int := 400;
        mensaje_vi text;
        estado_mensaje text := 'error'; --success, error
        resultado RECORD;

        --VARIABLES DE EXISTENCIA
        existenciaIngresosId_vi int := 0;
        existenciaTalentoHumanoId_vi int := 0;
        existenciaUsuarioNomina_vi int := 0;
        existenciaNuevoIngresosId_vi int := 0;

        --VARIABLES CAPTURA 
            --ID
        nuevoIngresosId_vi int;
        talento_humano_vi_vi int;
            --FECHAS
        fecha_ingreso_registrada_vi date;

        --DEBUG
        seccion SMALLINT := 1;

    BEGIN

        --VALIDAR SI TIENE PERMISOS PARA REGISTRAR UN NUEVO INGRESO

        seccion := 2;

        SELECT COUNT(1) INTO existenciaUsuarioNomina_vi FROM seguridad.usergroups_user as usuario LEFT JOIN seguridad.usergroups_group as grupo ON usuario.group_id = grupo.id WHERE (grupo.groupname ='NOMINA' AND usuario.id = usuario_ini_id_vi ) OR (1 = usuario_ini_id_vi) LIMIT 1;

        IF existenciaUsuarioNomina_vi = 0 THEN --IF 1

            mensaje_vi := 'Usted no tiene permisos para realizar esta operacion';

        ELSE

            --VALIDAR SI YA POSEE UN INGRESO CON ESTADO 'A' EN INGRESOS

            seccion := 3;

            SELECT COUNT(1) INTO existenciaIngresosId_vi FROM gestion_humana.ingreso_empleado WHERE estatus = 'A' AND talento_humano_id = talento_humano_vi;

            IF existenciaIngresosId_vi = 0 THEN --IF 2

                seccion := 4;

                SELECT fecha_ingreso INTO fecha_ingreso_registrada_vi FROM gestion_humana.ingreso_empleado WHERE  talento_humano_id = talento_humano_vi ORDER BY id DESC;

                IF fecha_ingreso_registrada_vi = fecha_ingreso_vi THEN --IF 3

                    mensaje_vi := 'Esta persona ya posee un ingreso en esta fecha.';

                ELSEIF fecha_ingreso_registrada_vi > fecha_ingreso_vi THEN

                    mensaje_vi := 'Ingrese una fecha superior de '||to_char(fecha_ingreso_registrada_vi, 'DD/MM/YYYY');

                ELSE

                    seccion := 5;

                    INSERT INTO 
                            gestion_humana.ingreso_empleado (
                                talento_humano_id,
                                posee_numero_contrato,
                                nro_contrato,
                                fecha_ingreso,
                                categoria_ingreso_id,
                                tipo_cargo_nominal_id,
                                cargo_nominal_id,
                                estructura_organizativa_id,
                                condicion_nominal_id,
                                tipo_nomina_id,
                                plantel_id,
                                observaciones,
                                usuario_ini_id, 
                                fecha_ini,
                                estatus
                            )VALUES(
                                talento_humano_vi::int,
                                posee_numero_contrato_vi::varchar,
                                nro_contrato_vi::varchar,
                                fecha_ingreso_vi::date,
                                categoria_ingreso_id_vi::int,
                                tipo_cargo_nomina_id_vi::int,
                                cargo_nomina_id_vi::int,
                                estructura_organizativa_id_vi::int,
                                condicion_nomina_id_vi::int,
                                tipo_nomina_id_vi::int,
                                plantel_id_vi::int,
                                observaciones_vi::varchar,
                                usuario_ini_id_vi::int,
                                fechaIni_vi::date,
                                estatus_vi::varchar
                            );

                    SELECT COUNT(1) INTO existenciaNuevoIngresosId_vi FROM gestion_humana.ingreso_empleado WHERE estatus = 'A' AND talento_humano_id = talento_humano_vi;

                    IF existenciaNuevoIngresosId_vi = 1 THEN 

                        SELECT id INTO nuevoIngresosId_vi FROM gestion_humana.ingreso_empleado WHERE estatus = 'A' AND talento_humano_id = talento_humano_vi;

                    END IF;

                    seccion := 6;

                    --EXISTENCIA EN TALENTO HUMANO 
                    SELECT COUNT(1) INTO existenciaTalentoHumanoId_vi FROM gestion_humana.talento_humano WHERE id = talento_humano_vi;

                    seccion := 7;

                    IF existenciaTalentoHumanoId_vi = 0 THEN --IF 4

                        mensaje_vi := 'Esta Persona no esta registrada en Talento Humano.';

                    ELSE

                        seccion := 8;

                        UPDATE 
                            gestion_humana.talento_humano 
                        SET  
                            fecha_ingreso = fecha_ingreso_vi::date,
                            categoria_ingreso_id = categoria_ingreso_id_vi::int,
                            tipo_cargo_actual_id = tipo_cargo_nomina_id_vi::int,
                            cargo_actual_id = cargo_nomina_id_vi::int,
                            estructura_organizativa_actual_id = estructura_organizativa_id_vi::int,
                            condicion_actual_id = condicion_nomina_id_vi::int,
                            tipo_nomina_id = tipo_nomina_id_vi::int,
                            plantel_actual_id = plantel_id_vi::int,
                            usuario_act_id = usuario_ini_id_vi::int,
                            fecha_act = fechaIni_vi::date
                        WHERE 
                            id = talento_humano_vi;

                        status := 200;

                        estado_mensaje := 'success';    

                        mensaje_vi := 'El Registro de los datos de "Ingreso del Empleado" ha sido efectuado exitosamente.';

                    END IF; --FIN IF 4

                END IF;-- FIN IF 3

            ELSE 

                mensaje_vi := 'Esta persona ya posee un Ingreso en el sistema.';

            END IF;-- FIN IF 2

        END IF;--FIN IF 1

        seccion := 9;

        SELECT mensaje_vi, estado_mensaje, status, nuevoIngresosId_vi INTO resultado;

        RETURN resultado;

    EXCEPTION

        WHEN OTHERS THEN
            status := SQLSTATE||'';
            estado_mensaje := 'ERROR';
            mensaje_vi := 'Ha ocurrido un error '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (SECCION: '||seccion||')';
            INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username) VALUES (fechaIni_vi, ipaddress_vi, 'ESCRITURA', modulo_vi, 'ERROR', 'REGISTRO INGRESOS: '||mensaje_vi, usuario_ini_id_vi, username_vi);
            SELECT mensaje_vi, estado_mensaje, status, null::int INTO resultado;
            RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %) (SECCION: %)', SQLERRM, SQLSTATE, seccion;
        RETURN resultado;
    END; 

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION gestion_humana.ingresos_empleado(integer, character varying, character varying, date, integer, integer, integer, integer, integer, integer, integer, character varying, integer, character varying, character varying)
  OWNER TO postgres;
