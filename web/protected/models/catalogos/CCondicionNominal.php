<?php
/**
 * Catalogo de $nombreClass
 *
 * @author Generador de Código
 */
class CCondicionNominal extends CCatalogo { 

    protected static $columns = 
        array (
  0 => 'id',
  1 => 'nombre',
  2 => 'siglas',
  3 => 'descripcion',
);

    /**
     * Setea la data en una propiedad static llamada data
     */
    protected static function setData(){

        self::$data = 
        array (
  0 => 
  array (
    'id' => 2,
    'nombre' => 'Alto Nivel y Dirección',
    'siglas' => 'AND',
    'descripcion' => NULL,
  ),
  1 => 
  array (
    'id' => 3,
    'nombre' => 'Fijo 99',
    'siglas' => 'F99',
    'descripcion' => NULL,
  ),
  2 => 
  array (
    'id' => 4,
    'nombre' => 'Personal Contratado',
    'siglas' => 'PC',
    'descripcion' => NULL,
  ),
  3 => 
  array (
    'id' => 1,
    'nombre' => 'Presidente',
    'siglas' => 'PR',
    'descripcion' => NULL,
  ),
)		; 

    	}
}
