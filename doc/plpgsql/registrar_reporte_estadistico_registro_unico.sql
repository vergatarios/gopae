
CREATE OR REPLACE FUNCTION rep_reg_unic.reporte_estadistico_registro_unico_diario(
fecha_i date,
hora_i time,
proceso_i varchar
)RETURNS RECORD AS $$
DECLARE 

    --VARIABLES EXISTENCIA
    count_rep_reg_unic int := 0;
    count_rep_detallado_plantel int := 0;
    count_rep_detallado_autoridad int := 0;
    count_rep_detallado_cocinera int := 0;

    --VARIABLES DE CAPTURA
    captura_log_id int;

    --VARIABLES RESULTADO
    respuesta RECORD;

    --MENSAJES LOG
    resultado_vi varchar := 'ERROR'; -- EXITOSO - ERROR
    estatus_vi char := 'I'; -- A - I
    cod_error_vi varchar := null;
    mensaje_vi text := null;


    --VARIABLES DE EXCEPTION 
    i INTEGER := 1;


    /*
    La estructura del log Es la siguiente:

    ID \ PROCESO \ RESULTADO \ HORA \ FECHA \ ESTATUS \ COD_ERROR \ MENSAJE_ERROR
       \         \           \      \       \         \           \



    LISTA DE RESULTADOS:

    PROCESO     \  RESULTADO \ ESTATUS \ COD_ERROR \ MENSAJE_ERROR
    //////////////////////////////////////////////////////////////
    ESTADISTICO \  EXITOSO   \    A    \           \
    ESTADISTICO \  ERROR     \    I    \           \ La consulta de Reporte Estadistico no muestra resultados. No es posible obtener la data.


    */

BEGIN

    --VALIDAR QUE EL SELECT FUNCIONA CORRECTAMENTE
    SELECT COUNT (1) INTO count_rep_reg_unic FROM (
        SELECT  e.region_id AS region_id, r.nombre AS region, e.id AS id, e.nombre, 'AAA'||e.nombre AS titulo
            , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
            , SUM(CASE WHEN (p.cod_cnae IS NOT NULL AND p.es_beneficiario_pae = 'SI' AND pp.matricula_general>0) THEN 1 ELSE 0 END) AS total_cnae
            , (SELECT COUNT(DISTINCT ap.usuario_id) FROM gplantel.autoridad_plantel ap INNER JOIN gplantel.plantel ppa ON ppa.es_beneficiario_pae = 'SI' AND ap.plantel_id = ppa.id INNER JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id WHERE u.presento_documento_identidad = 'SI' AND u.foto IS NOT NULL AND ap.cargo_id IN (3, 5, 27) AND ap.estatus = 'A' AND ppa.estado_id = e.id) AS autoridades_verificados
            , (SELECT COUNT(DISTINCT ap.usuario_id) FROM gplantel.autoridad_plantel ap INNER JOIN gplantel.plantel ppa ON ppa.es_beneficiario_pae = 'SI' AND ap.plantel_id = ppa.id INNER JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id WHERE u.presento_documento_identidad = 'SI' AND (u.foto IS NULL OR u.foto='') AND ap.cargo_id IN (3, 5, 27) AND ap.estatus = 'A' AND ppa.estado_id = e.id) AS autoridades_verificados_sin_foto
            , (SELECT COUNT(th.id) FROM gestion_humana.talento_humano th WHERE th.estado_id = e.id AND th.estatus = 'E' AND th.tipo_cargo_actual_id = 11) AS cocineras_escolares
            , (SELECT COUNT(c.id) FROM gplantel.cocinera_plantel c INNER JOIN gplantel.plantel pc ON c.plantel_id = pc.id WHERE pc.estado_id = e.id AND c.estatus = 'A') AS cocineras_escolares_asignadas
            , SUM(CASE WHEN (pp.proveedor_actual_id = 5 AND p.es_beneficiario_pae = 'SI') THEN 1 ELSE 0 END) AS beneficiados_x_mercal
            , SUM(CASE WHEN (pp.proveedor_actual_id = 6 AND p.es_beneficiario_pae = 'SI') THEN 1 ELSE 0 END) AS beneficiados_x_pdval
            , SUM(CASE WHEN ((p.es_beneficiario_pae = 'SI' AND pp.proveedor_actual_id != 6 AND pp.proveedor_actual_id != 5) OR (p.es_beneficiario_pae = 'SI' AND pp.proveedor_actual_id IS NULL)) THEN 1 ELSE 0 END) AS beneficiados_x_otros

        FROM public.region r
            INNER JOIN public.estado e
                ON r.id = e.region_id
            LEFT JOIN gplantel.plantel p
                ON e.id = p.estado_id
            LEFT JOIN gplantel.plantel_pae pp
                ON pp.plantel_id = p.id
        GROUP BY
            e.region_id, r.nombre, e.id, e.nombre, titulo
        UNION

            SELECT  0 AS region_id, 'NACIONAL' AS region, 0 AS id, 'TOTAL', 'ZZZTOTAL' AS titulo
                , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
                , SUM(CASE WHEN (p.cod_cnae IS NOT NULL AND p.es_beneficiario_pae = 'SI' AND pp.matricula_general>0) THEN 1 ELSE 0 END) AS total_cnae
                , (SELECT COUNT(DISTINCT ap.usuario_id) FROM gplantel.autoridad_plantel ap INNER JOIN gplantel.plantel ppa ON ppa.es_beneficiario_pae = 'SI' AND ap.plantel_id = ppa.id INNER JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id WHERE u.presento_documento_identidad = 'SI' AND u.foto IS NOT NULL AND ap.cargo_id IN (3, 5, 27) AND ap.estatus = 'A') AS autoridades_verificados
                , (SELECT COUNT(DISTINCT ap.usuario_id) FROM gplantel.autoridad_plantel ap INNER JOIN gplantel.plantel ppa ON ppa.es_beneficiario_pae = 'SI' AND ap.plantel_id = ppa.id INNER JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id WHERE u.presento_documento_identidad = 'SI' AND (u.foto IS NULL OR u.foto='') AND ap.cargo_id IN (3, 5, 27) AND ap.estatus = 'A') AS autoridades_verificados_sin_foto
                , (SELECT COUNT(th.id) FROM gestion_humana.talento_humano th WHERE th.estatus = 'E' AND th.tipo_cargo_actual_id = 11) AS cocineras_escolares
                , (SELECT COUNT(c.id) FROM gplantel.cocinera_plantel c INNER JOIN gplantel.plantel pc ON c.plantel_id = pc.id WHERE c.estatus = 'A') AS cocineras_escolares_asignadas
                , SUM(CASE WHEN (pp.proveedor_actual_id = 5 AND p.es_beneficiario_pae = 'SI') THEN 1 ELSE 0 END) AS beneficiados_x_mercal
                , SUM(CASE WHEN (pp.proveedor_actual_id = 6 AND p.es_beneficiario_pae = 'SI') THEN 1 ELSE 0 END) AS beneficiados_x_pdval
                , SUM(CASE WHEN ((p.es_beneficiario_pae = 'SI' AND pp.proveedor_actual_id != 6 AND pp.proveedor_actual_id != 5) OR (p.es_beneficiario_pae = 'SI' AND pp.proveedor_actual_id IS NULL)) THEN 1 ELSE 0 END) AS beneficiados_x_otros
            FROM gplantel.plantel p
            LEFT JOIN gplantel.plantel_pae pp ON pp.plantel_id = p.id
            ORDER BY titulo ASC, nombre ASC
    ) as validar;
    
    IF count_rep_reg_unic > 23 THEN


        --SE LIMPIA LA TABLA
        TRUNCATE TABLE rep_reg_unic.estadistico;
        --SE REINICIA EL CONTADOR DEL ID
         ALTER SEQUENCE rep_reg_unic.estadistico_id_seq restart 1;


        --INICIA EL INSERT EN LA TABLA
        INSERT INTO rep_reg_unic.estadistico (
                region_id,
                region,
                estado_id,
                estado_nombre,
                titulo,
                planteles,
                total_cnae,
                autoridades_verificados,
                autoridades_verificados_sin_foto,
                cocineras_escolares,
                cocineras_escolares_asignadas,
                beneficiados_x_mercal,
                beneficiados_x_pdval,
                beneficiados_x_otros,
                fecha,
                hora
        ) 

        SELECT  e.region_id AS region_id, r.nombre AS region, e.id AS id, e.nombre, 'AAA'||e.nombre AS titulo
                , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
                , SUM(CASE WHEN (p.cod_cnae IS NOT NULL AND p.es_beneficiario_pae = 'SI' AND pp.matricula_general>0) THEN 1 ELSE 0 END) AS total_cnae
                , (SELECT COUNT(DISTINCT ap.usuario_id) FROM gplantel.autoridad_plantel ap INNER JOIN gplantel.plantel ppa ON ppa.es_beneficiario_pae = 'SI' AND ap.plantel_id = ppa.id INNER JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id WHERE u.presento_documento_identidad = 'SI' AND u.foto IS NOT NULL AND ap.cargo_id IN (3, 5, 27) AND ap.estatus = 'A' AND ppa.estado_id = e.id) AS autoridades_verificados
                , (SELECT COUNT(DISTINCT ap.usuario_id) FROM gplantel.autoridad_plantel ap INNER JOIN gplantel.plantel ppa ON ppa.es_beneficiario_pae = 'SI' AND ap.plantel_id = ppa.id INNER JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id WHERE u.presento_documento_identidad = 'SI' AND (u.foto IS NULL OR u.foto='') AND ap.cargo_id IN (3, 5, 27) AND ap.estatus = 'A' AND ppa.estado_id = e.id) AS autoridades_verificados_sin_foto
                , (SELECT COUNT(th.id) FROM gestion_humana.talento_humano th WHERE th.estado_id = e.id AND th.estatus = 'E' AND th.tipo_cargo_actual_id = 11) AS cocineras_escolares
                , (SELECT COUNT(c.id) FROM gplantel.cocinera_plantel c INNER JOIN gplantel.plantel pc ON c.plantel_id = pc.id WHERE pc.estado_id = e.id AND c.estatus = 'A') AS cocineras_escolares_asignadas
                , SUM(CASE WHEN (pp.proveedor_actual_id = 5 AND p.es_beneficiario_pae = 'SI') THEN 1 ELSE 0 END) AS beneficiados_x_mercal
                , SUM(CASE WHEN (pp.proveedor_actual_id = 6 AND p.es_beneficiario_pae = 'SI') THEN 1 ELSE 0 END) AS beneficiados_x_pdval
                , SUM(CASE WHEN ((p.es_beneficiario_pae = 'SI' AND pp.proveedor_actual_id != 6 AND pp.proveedor_actual_id != 5) OR (p.es_beneficiario_pae = 'SI' AND pp.proveedor_actual_id IS NULL)) THEN 1 ELSE 0 END) AS beneficiados_x_otros
                , fecha_i
                , hora_i
                    
            FROM public.region r
                INNER JOIN public.estado e
                    ON r.id = e.region_id
                LEFT JOIN gplantel.plantel p
                    ON e.id = p.estado_id
                LEFT JOIN gplantel.plantel_pae pp
                    ON pp.plantel_id = p.id
            GROUP BY
                e.region_id, r.nombre, e.id, e.nombre, titulo
            UNION

                SELECT  0 AS region_id, 'NACIONAL' AS region, 0 AS id, 'TOTAL', 'ZZZTOTAL' AS titulo
                    , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
                    , SUM(CASE WHEN (p.cod_cnae IS NOT NULL AND p.es_beneficiario_pae = 'SI' AND pp.matricula_general>0) THEN 1 ELSE 0 END) AS total_cnae
                    , (SELECT COUNT(DISTINCT ap.usuario_id) FROM gplantel.autoridad_plantel ap INNER JOIN gplantel.plantel ppa ON ppa.es_beneficiario_pae = 'SI' AND ap.plantel_id = ppa.id INNER JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id WHERE u.presento_documento_identidad = 'SI' AND u.foto IS NOT NULL AND ap.cargo_id IN (3, 5, 27) AND ap.estatus = 'A') AS autoridades_verificados
                    , (SELECT COUNT(DISTINCT ap.usuario_id) FROM gplantel.autoridad_plantel ap INNER JOIN gplantel.plantel ppa ON ppa.es_beneficiario_pae = 'SI' AND ap.plantel_id = ppa.id INNER JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id WHERE u.presento_documento_identidad = 'SI' AND (u.foto IS NULL OR u.foto='') AND ap.cargo_id IN (3, 5, 27) AND ap.estatus = 'A') AS autoridades_verificados_sin_foto
                    , (SELECT COUNT(th.id) FROM gestion_humana.talento_humano th WHERE th.estatus = 'E' AND th.tipo_cargo_actual_id = 11) AS cocineras_escolares
                    , (SELECT COUNT(c.id) FROM gplantel.cocinera_plantel c INNER JOIN gplantel.plantel pc ON c.plantel_id = pc.id WHERE c.estatus = 'A') AS cocineras_escolares_asignadas
                    , SUM(CASE WHEN (pp.proveedor_actual_id = 5 AND p.es_beneficiario_pae = 'SI') THEN 1 ELSE 0 END) AS beneficiados_x_mercal
                    , SUM(CASE WHEN (pp.proveedor_actual_id = 6 AND p.es_beneficiario_pae = 'SI') THEN 1 ELSE 0 END) AS beneficiados_x_pdval
                    , SUM(CASE WHEN ((p.es_beneficiario_pae = 'SI' AND pp.proveedor_actual_id != 6 AND pp.proveedor_actual_id != 5) OR (p.es_beneficiario_pae = 'SI' AND pp.proveedor_actual_id IS NULL)) THEN 1 ELSE 0 END) AS beneficiados_x_otros
                    , fecha_i
                    , hora_i

                FROM gplantel.plantel p
                LEFT JOIN gplantel.plantel_pae pp ON pp.plantel_id = p.id
                ORDER BY titulo ASC, nombre ASC;

        resultado_vi := 'EXITOSO';
        estatus_vi := 'A';
        UPDATE rep_reg_unic.log_estado SET estatus = 'I' WHERE estatus = 'A';
                
    ELSE 
        resultado_vi := 'ERROR';
        estatus_vi := 'I';
        cod_error_vi := 'E0001';
        mensaje_vi := 'La consulta de Reporte Estadistico no muestra resultados. No es posible obtener la data.';
    END IF;

    

    INSERT INTO rep_reg_unic.log_estado (proceso, resultado, hora, fecha, estatus, cod_error, mensaje) VALUES (proceso_i, resultado_vi, hora_i, fecha_i, estatus_vi, cod_error_vi, mensaje_vi) RETURNING id INTO captura_log_id;


    --VALIDAR SI EL DETALLADO DE PLANTELES REGRESA DATA
    SELECT count(1) INTO count_rep_detallado_plantel
    FROM (
        SELECT 
            p.cod_plantel,
            p.cod_estadistico,
            p.nombre AS nombre_plantel,
            p.annio_fundado,
            p.cod_cnae,
            p.codigo_ner,
            p.registro_cnae,
            d.nombre AS denominacion,
            td.nombre AS dependencia,
            e.nombre AS estado,
            e.capital AS estado_capital,
            m.nombre AS municipio,
            m.capital AS municipio_capital,
            z.nombre AS zona_educativa,
            pp.pae_activo,
            ts.nombre AS tipo_servicio_pae,
            (SELECT string_agg(tm.nombre, ' | ') FROM gplantel.plantel_ingesta pi INNER JOIN nutricion.tipo_menu tm ON pi.tipo_ingesta_id = tm.id GROUP BY pi.plantel_id HAVING pi.plantel_id = p.id) AS ingestas,
            pp.cantidad_madres_procesadoras,
            pp.matricula_maternal,
            pp.matricula_preescolar,
            pp.matricula_educacion_primaria,
            pp.matricula_educacion_media_general,
            pp.matricula_educacion_tecnica,
            u.origen AS origen_director,
            u.cedula AS cedula_director,
            u.apellido AS apellido_director,
            u.nombre AS nombre_director,
            u.telefono AS telefono_director,
            u.email AS email_director,
            (SELECT ec.origen FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS origen_enlace_pae,
            (SELECT ec.cedula FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS cedula_enlace_pae,
            (SELECT ec.apellido FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS apellido_enlace_cnae,
            (SELECT ec.nombre FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS nombre_enlace_cnae,
            (SELECT ec.telefono FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS telefono_enlace_cnae,
            (SELECT ec.email FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS email_enlace_cnae

        FROM
            gplantel.plantel p
            LEFT JOIN gplantel.plantel_pae pp ON pp.plantel_id = p.id
            LEFT JOIN public.estado e ON p.estado_id = e.id
            LEFT JOIN gplantel.zona_educativa z ON p.zona_educativa_id = z.id
            LEFT JOIN public.municipio m ON p.municipio_id = m.id
            LEFT JOIN seguridad.usergroups_user u ON p.director_actual_id = u.id
            LEFT JOIN gplantel.denominacion d ON p.denominacion_id = d.id
            LEFT JOIN gplantel.tipo_dependencia td ON p.tipo_dependencia_id = td.id
            LEFT JOIN gplantel.tipo_servicio_pae ts ON pp.tipo_servicio_pae_id = ts.id
        ) as validar;
    IF (count_rep_detallado_plantel > 0) and (resultado_vi = 'EXITOSO') THEN 

    
        -- INICIO DEL INSERT

        --SE LIMPIA LA TABLA
        TRUNCATE TABLE rep_reg_unic.detalle_plantel;
        --SE REINICIA EL CONTADOR DEL ID
        ALTER SEQUENCE rep_reg_unic.detalle_plantel_id_seq restart 1;


        INSERT INTO rep_reg_unic.detalle_plantel (
            cod_plantel,
            cod_estadistico,
            nombre_plantel,
            annio_fundado,
            cod_cnae,
            codigo_ner,
            registro_cnae,
            denominacion,
            dependencia,
            estado,
            estado_capital,
            municipio,
            municipio_capital,
            zona_educativa,
            pae_activo,
            tipo_servicio_pae,
            ingestas,
            cantidad_madres_procesadoras,
            matricula_maternal,
            matricula_preescolar,
            matricula_educacion_primaria,
            matricula_educacion_media_general,
            matricula_educacion_tecnica,
            origen_director,
            cedula_director,
            apellido_director,
            nombre_director,
            telefono_director,
            email_director,
            origen_enlace_pae,
            cedula_enlace_pae,
            apellido_enlace_cnae,
            nombre_enlace_cnae,
            telefono_enlace_cnae,
            email_enlace_cnae,
            plantel_id,
            es_beneficiario_pae,
            matricula_general,
            proveedor_actual_id,
            log_id
            )
            SELECT 
                p.cod_plantel,
                p.cod_estadistico,
                p.nombre AS nombre_plantel,
                p.annio_fundado,
                p.cod_cnae,
                p.codigo_ner,
                p.registro_cnae,
                d.nombre AS denominacion,
                td.nombre AS dependencia,
                e.nombre AS estado,
                e.capital AS estado_capital,
                m.nombre AS municipio,
                m.capital AS municipio_capital,
                z.nombre AS zona_educativa,
                pp.pae_activo,
                ts.nombre AS tipo_servicio_pae,
                (SELECT string_agg(tm.nombre, ' | ') FROM gplantel.plantel_ingesta pi INNER JOIN nutricion.tipo_menu tm ON pi.tipo_ingesta_id = tm.id GROUP BY pi.plantel_id HAVING pi.plantel_id = p.id) AS ingestas,
                pp.cantidad_madres_procesadoras,
                pp.matricula_maternal,
                pp.matricula_preescolar,
                pp.matricula_educacion_primaria,
                pp.matricula_educacion_media_general,
                pp.matricula_educacion_tecnica,
                u.origen AS origen_director,
                u.cedula AS cedula_director,
                u.apellido AS apellido_director,
                u.nombre AS nombre_director,
                u.telefono AS telefono_director,
                u.email AS email_director,
                (SELECT ec.origen FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS origen_enlace_pae,
                (SELECT ec.cedula FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS cedula_enlace_pae,
                (SELECT ec.apellido FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS apellido_enlace_cnae,
                (SELECT ec.nombre FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS nombre_enlace_cnae,
                (SELECT ec.telefono FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS telefono_enlace_cnae,
                (SELECT ec.email FROM gplantel.autoridad_plantel ap INNER JOIN seguridad.usergroups_user ec ON ap.usuario_id = ec.id WHERE ap.plantel_id = p.id AND ap.cargo_id = 27 AND ap.estatus = 'A') AS email_enlace_cnae,
                p.id as plantel_id,
                p.es_beneficiario_pae,
                pp.matricula_general,
                pp.proveedor_actual_id,
                captura_log_id
		
            FROM
                gplantel.plantel p
                LEFT JOIN gplantel.plantel_pae pp ON pp.plantel_id = p.id
                LEFT JOIN public.estado e ON p.estado_id = e.id
                LEFT JOIN gplantel.zona_educativa z ON p.zona_educativa_id = z.id
                LEFT JOIN public.municipio m ON p.municipio_id = m.id
                LEFT JOIN seguridad.usergroups_user u ON p.director_actual_id = u.id
                LEFT JOIN gplantel.denominacion d ON p.denominacion_id = d.id
                LEFT JOIN gplantel.tipo_dependencia td ON p.tipo_dependencia_id = td.id
                LEFT JOIN gplantel.tipo_servicio_pae ts ON pp.tipo_servicio_pae_id = ts.id;
		
    END IF;

    SELECT COUNT(1) INTO count_rep_detallado_autoridad
    FROM (  SELECT p.cod_plantel,
        p.cod_estadistico,
        p.nombre AS nombre_plantel,
        p.annio_fundado,
        p.cod_cnae,
        p.codigo_ner,
        p.registro_cnae,
        d.nombre AS denominacion,
        td.nombre AS dependencia,
        e.nombre AS estado,
        e.capital AS estado_capital,
        m.nombre AS municipio,
        m.capital AS municipio_capital,
        z.nombre AS zona_educativa,
        pp.pae_activo,
        ts.nombre AS tipo_servicio_pae,
        (SELECT string_agg(tm.nombre, ' | ') FROM gplantel.plantel_ingesta pi INNER JOIN nutricion.tipo_menu tm ON pi.tipo_ingesta_id = tm.id GROUP BY pi.plantel_id HAVING pi.plantel_id = p.id) AS ingestas,
        pp.cantidad_madres_procesadoras,
        pp.matricula_maternal,
        pp.matricula_preescolar,
        pp.matricula_educacion_primaria,
        pp.matricula_educacion_media_general,
        pp.matricula_educacion_tecnica,
        c.nombre AS responsabilidad_autoridad,
        u.origen AS origen_autoridad,
        u.cedula AS cedula_autoridad,
        u.apellido AS apellido_autoridad,
        u.nombre AS nombre_autoridad,
        u.telefono AS telefono_autoridad,
        u.email AS email_autoridad,
        u.foto AS foto,
        p.es_beneficiario_pae,
        u.presento_documento_identidad,
        p.director_actual_id,
        p.subdirector_actual_id,
        p.enlace_pae_actual_id,
        ap.estatus,
        ap.cargo_id
    FROM
        gplantel.plantel p
        LEFT JOIN gplantel.plantel_pae pp ON pp.plantel_id = p.id
        LEFT JOIN public.estado e ON p.estado_id = e.id
        LEFT JOIN gplantel.zona_educativa z ON p.zona_educativa_id = z.id
        LEFT JOIN public.municipio m ON p.municipio_id = m.id
        LEFT JOIN gplantel.denominacion d ON p.denominacion_id = d.id
        LEFT JOIN gplantel.tipo_dependencia td ON p.tipo_dependencia_id = td.id
        LEFT JOIN gplantel.tipo_servicio_pae ts ON pp.tipo_servicio_pae_id = ts.id
        INNER JOIN gplantel.autoridad_plantel ap ON ap.plantel_id = p.id
        LEFT JOIN gplantel.cargo c ON ap.cargo_id = c.id
        LEFT JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id
    )   as validar;


    IF (count_rep_detallado_autoridad > 0) and (resultado_vi = 'EXITOSO') THEN 


	-- INICIO DEL INSERT

        --SE LIMPIA LA TABLA
        TRUNCATE TABLE rep_reg_unic.detalle_autoridad;
        --SE REINICIA EL CONTADOR DEL ID
        ALTER SEQUENCE rep_reg_unic.detalle_autoridad_id_seq restart 1;
    
        INSERT INTO rep_reg_unic.detalle_autoridad(
            cod_plantel,
            cod_estadistico,
            nombre_plantel,
            annio_fundado,
            cod_cnae,
            codigo_ner,
            registro_cnae,
            denominacion,
            dependencia,
            estado,
            estado_capital,
            municipio,
            municipio_capital,
            zona_educativa,
            pae_activo,
            tipo_servicio_pae,
            ingestas,
            cantidad_madres_procesadoras,
            matricula_maternal,
            matricula_preescolar,
            matricula_educacion_primaria,
            matricula_educacion_media_general,
            matricula_educacion_tecnica,
            responsabilidad_autoridad,
            origen_autoridad,
            cedula_autoridad,
            apellido_autoridad,
            nombre_autoridad,
            telefono_autoridad,
            email_autoridad,
            foto,
            es_beneficiario_pae,
            presento_documento_identidad,
            director_actual_id,
            subdirector_actual_id,
            enlace_pae_actual_id,
            estatus,
            cargo_id,
            log_id
        ) 
        SELECT p.cod_plantel,
            p.cod_estadistico,
            p.nombre AS nombre_plantel,
            p.annio_fundado,
            p.cod_cnae,
            p.codigo_ner,
            p.registro_cnae,
            d.nombre AS denominacion,
            td.nombre AS dependencia,
            e.nombre AS estado,
            e.capital AS estado_capital,
            m.nombre AS municipio,
            m.capital AS municipio_capital,
            z.nombre AS zona_educativa,
            pp.pae_activo,
            ts.nombre AS tipo_servicio_pae,
            (SELECT string_agg(tm.nombre, ' | ') FROM gplantel.plantel_ingesta pi INNER JOIN nutricion.tipo_menu tm ON pi.tipo_ingesta_id = tm.id GROUP BY pi.plantel_id HAVING pi.plantel_id = p.id) AS ingestas,
            pp.cantidad_madres_procesadoras,
            pp.matricula_maternal,
            pp.matricula_preescolar,
            pp.matricula_educacion_primaria,
            pp.matricula_educacion_media_general,
            pp.matricula_educacion_tecnica,
            c.nombre AS responsabilidad_autoridad,
            u.origen AS origen_autoridad,
            u.cedula AS cedula_autoridad,
            u.apellido AS apellido_autoridad,
            u.nombre AS nombre_autoridad,
            u.telefono AS telefono_autoridad,
            u.email AS email_autoridad,
            u.foto AS foto,
            p.es_beneficiario_pae,
            u.presento_documento_identidad,
            p.director_actual_id,
            p.subdirector_actual_id,
            p.enlace_pae_actual_id,
            ap.estatus,
            ap.cargo_id,
            captura_log_id
        FROM
            gplantel.plantel p
            LEFT JOIN gplantel.plantel_pae pp ON pp.plantel_id = p.id
            LEFT JOIN public.estado e ON p.estado_id = e.id
            LEFT JOIN gplantel.zona_educativa z ON p.zona_educativa_id = z.id
            LEFT JOIN public.municipio m ON p.municipio_id = m.id
            LEFT JOIN gplantel.denominacion d ON p.denominacion_id = d.id
            LEFT JOIN gplantel.tipo_dependencia td ON p.tipo_dependencia_id = td.id
            LEFT JOIN gplantel.tipo_servicio_pae ts ON pp.tipo_servicio_pae_id = ts.id
            INNER JOIN gplantel.autoridad_plantel ap ON ap.plantel_id = p.id
            LEFT JOIN gplantel.cargo c ON ap.cargo_id = c.id
            LEFT JOIN seguridad.usergroups_user u ON ap.usuario_id = u.id;
    END IF;

    SELECT COUNT(1) FROM INTO count_rep_detallado_cocinera (
        SELECT
                eth.nombre AS estado_cocinera_escolar,
                th.origen,
                th.cedula,
                th.nombre,
                th.apellido,
                th.fecha_nacimiento,
                th.sexo,
                th.telefono_celular,
                th.telefono_fijo,
                th.email_personal,
                p.cod_plantel,
                p.cod_estadistico,
                p.nombre AS nombre_plantel,
                p.annio_fundado,
                p.cod_cnae,
                p.codigo_ner,
                p.registro_cnae,
                d.nombre AS denominacion,
                td.nombre AS dependencia,
                e.nombre AS estado,
                e.capital AS estado_capital,
                m.nombre AS municipio,
                u.origen AS origen_director,
                u.cedula AS cedula_director,
                u.apellido AS apellido_director,
                u.nombre AS nombre_director,
                u.telefono AS telefono_director,
                u.email AS email_director,
                th.estatus,
                th.tipo_cargo_actual_id,
                th.plantel_actual_id
                


            FROM
                gestion_humana.talento_humano th
                LEFT JOIN gplantel.plantel p ON th.plantel_actual_id = p.id
                LEFT JOIN gplantel.plantel_pae pp ON pp.plantel_id = p.id
                LEFT JOIN public.estado e ON p.estado_id = e.id
                LEFT JOIN public.estado eth ON th.estado_id = eth.id
                LEFT JOIN public.municipio m ON p.municipio_id = m.id
                LEFT JOIN seguridad.usergroups_user u ON p.director_actual_id = u.id
                LEFT JOIN gplantel.denominacion d ON p.denominacion_id = d.id
                LEFT JOIN gplantel.tipo_dependencia td ON p.tipo_dependencia_id = td.id ) as validar;

    IF (count_rep_detallado_cocinera > 0) and (resultado_vi = 'EXITOSO') THEN 

        -- INICIO DEL INSERT

        --SE LIMPIA LA TABLA
        TRUNCATE TABLE rep_reg_unic.detalle_cocinera;
        --SE REINICIA EL CONTADOR DEL ID
        ALTER SEQUENCE rep_reg_unic.detalle_cocinera_id_seq restart 1;


        INSERT INTO rep_reg_unic.detalle_cocinera (
            estado_cocinera_escolar,
            origen,
            cedula,
            nombre,
            apellido,
            fecha_nacimiento,
            sexo,
            telefono_celular,
            telefono_fijo,
            email_personal,
            cod_plantel,
            cod_estadistico,
            nombre_plantel,
            annio_fundado,
            cod_cnae,
            codigo_ner,
            registro_cnae,
            denominacion,
            dependencia,
            estado,
            estado_capital,
            municipio,
            origen_director,
            cedula_director,
            apellido_director,
            nombre_director,
            telefono_director,
            email_director,
            estatus,
            tipo_cargo_actual_id,
            plantel_actual_id,
            log_id
        )   SELECT
                eth.nombre AS estado_cocinera_escolar,
                th.origen,
                th.cedula,
                th.nombre,
                th.apellido,
                th.fecha_nacimiento,
                th.sexo,
                th.telefono_celular,
                th.telefono_fijo,
                th.email_personal,
                p.cod_plantel,
                p.cod_estadistico,
                p.nombre AS nombre_plantel,
                p.annio_fundado,
                p.cod_cnae,
                p.codigo_ner,
                p.registro_cnae,
                d.nombre AS denominacion,
                td.nombre AS dependencia,
                e.nombre AS estado,
                e.capital AS estado_capital,
                m.nombre AS municipio,
                u.origen AS origen_director,
                u.cedula AS cedula_director,
                u.apellido AS apellido_director,
                u.nombre AS nombre_director,
                u.telefono AS telefono_director,
                u.email AS email_director,
                th.estatus,
                th.tipo_cargo_actual_id,
                th.plantel_actual_id,
                captura_log_id


            FROM
                gestion_humana.talento_humano th
                LEFT JOIN gplantel.plantel p ON th.plantel_actual_id = p.id
                LEFT JOIN gplantel.plantel_pae pp ON pp.plantel_id = p.id
                LEFT JOIN public.estado e ON p.estado_id = e.id
                LEFT JOIN public.estado eth ON th.estado_id = eth.id
                LEFT JOIN public.municipio m ON p.municipio_id = m.id
                LEFT JOIN seguridad.usergroups_user u ON p.director_actual_id = u.id
                LEFT JOIN gplantel.denominacion d ON p.denominacion_id = d.id
                LEFT JOIN gplantel.tipo_dependencia td ON p.tipo_dependencia_id = td.id;

    END IF;
RETURN respuesta;

EXCEPTION
    WHEN OTHERS THEN
        resultado_vi := 'ERROR';
        estatus_vi := 'I';
        mensaje_vi := 'Ha ocurrido un error '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (LINEA: '||line||')';
        respuesta := ARRAY[dato_vi, resultado_vi, mensaje_v];
        INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username) VALUES (fecha_v, ipaddress_vi, 'ESCRITURA', modulo_vi, 'ERROR', 'REGISTRO INICIAL DE REGISTRO: '||mensaje_v, usuario_vi, username_vi);
        RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %) (LINEA: %)', SQLERRM, SQLSTATE, line;
        INSERT INTO rep_reg_unic.log_estado (proceso, resultado, hora, fecha, estatus, cod_error, mensaje) VALUES (proceso_i, resultado_vi, hora_i, fecha_i, estatus_vi, SQLSTATE, mensaje_v) RETURNING id INTO captura_log_id;

    RETURN respuesta;
END;$$ LANGUAGE plpgsql;